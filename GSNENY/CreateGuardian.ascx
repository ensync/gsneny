﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CreateGuardian.ascx.cs" Inherits="GSNENY.CreateAdultContact" %>
<style> 
label.field-wrapper input {
    float: right;
}
label.required-field::before { 
    content: "*";
    float: left;
    color: red;
}
    .empty{
        border-color:red !important;
    }
</style>
 <div>
        <label style="color: red; display: none" id="ErrorLabel" />
    </div>
    <br>
    <br>
    <div>
      <label  class="field-wrapper required-field col-sm-3"> First Name </label> <input class="required" type="text" id="FirstName" value=""><br><br>
      <label  class="field-wrapper required-field col-sm-3"> Last Name </label><input class="required" type="text" id="LastName" value="" ><br><br>
      <label  class="field-wrapper required-field col-sm-3"> Email </label><input class="required" type="text" id="Email" value=""><br><br>
      <label  class="field-wrapper required-field col-sm-3"> Address 1 </label><input class="required" type="text" id="Address1"><br><br>
      <label  class="col-sm-3"> Address 2 </label><input type="text" id="Address2"><br><br>
      <label  class="field-wrapper required-field col-sm-3"> City </label><input class="required" type="text" id="City" value=""><br><br>
      <label  class="field-wrapper required-field col-sm-3">State </label><select class="required" id="State" ></select><br><br>
      <label  class="field-wrapper required-field col-sm-3">Zip </label><input class="required" type="text" id="Zip" value=""><br><br>
      <label  class="field-wrapper required-field col-sm-3"> Username </label><input class="required" type="text" id="Username" value=""><br><br>
      <label  class="field-wrapper required-field col-sm-3"> Password </label><input class="required" type="password" id="Password" value=""><br><br>
        <label  class="field-wrapper required-field col-sm-3"> Confirm Password </label><input class="required" type="password" id="ConfirmPassword" value="" onChange="checkPasswordMatch();"><br><br>
    </div>
    <div class="col-md-8 col-md-offset-4">
        <input type="button" class="PrimaryButton pull-right TextButton" id="finishButton" value="Save" style="margin-left: 10px">
        <br />
    </div>
<script>
    
    jQuery(document).ready(function (jQuery) {
        PopulateStates();
        jQuery("#Email").on('keydown', function () {
            jQuery("#Username").val(jQuery(this).val());
        });
    });

    var PopulateStates = function () {
        statesDropDown = jQuery('#State');
        jQuery.ajax({
            type: "POST",
            url: "/GSNENY/Membership/Bridge/Bridge.aspx/PopulateStates",
            contentType: "application/json;",
            success: function (response) {
                //console.log(response.d);
                if (response.d != null) {
                    statesDropDown.append(jQuery("<option />").val(("")).text(""));
                    jQuery.each(response.d, function (index, value) {
                        statesDropDown.append(jQuery("<option />").val(value).text(index));
                    });
                }
                AutoPopulateInformation();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var obj = JSON.parse(jqXHR.responseText);
                console.log(obj.Message);
                console.log(jqXHR.statusText);
            }
        });
    }

    var AutoPopulateInformation = function () {
        var dataToSend = JSON.stringify({
            'ID': localStorage.getItem('girlID')
        });
        jQuery.ajax({
            type: "POST",
            url: "/GSNENY/Membership/Bridge/Bridge.aspx/AutoPopulateInformation",
            contentType: "application/json;",
            dataType: "json",
            data: dataToSend,
            success: function (response) {
                //console.log(response.d);
                if (response.d != null) {
                    if (response.d.Address1 != null) jQuery('#Address1').val(response.d.Address1);
                    if (response.d.Address2 != null) jQuery('#Address2').val(response.d.Address2);
                    if (response.d.City != null) jQuery('#City').val(response.d.City);
                    if (response.d.State != null) jQuery('#State').val(response.d.State);
                    if (response.d.Zip != null) jQuery('#Zip').val(response.d.Zip);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var obj = JSON.parse(jqXHR.responseText);
                console.log(obj.Message);
                console.log(jqXHR.statusText);
            }
        });
    }

    function checkPasswordMatch() {
        var password = jQuery("#Password").val();
        var confirmPassword = jQuery("#ConfirmPassword").val();

        if (password != confirmPassword) {
            jQuery("#ErrorLabel").html("The passwords don't match. Try again?");
            jQuery("#ErrorLabel").show();
            jQuery('#Password').addClass('empty');
            jQuery('#ConfirmPassword').addClass('empty');
        }
        else {
            jQuery("#ErrorLabel").hide();
            jQuery('#Password').removeClass('empty');
            jQuery('#ConfirmPassword').removeClass('empty');
        }
    }

    //on click function for 'tes, I want to add another contact'
    //jQuery('#submitButton').on('click', function () {
    //    var emptyParameters = [];
    //    //checking if all elements with required class are entered
    //    jQuery('.required').each(function () {
    //        if (this.value.length <= 0) {
    //            emptyParameters.push(this.id);
    //        }
    //    });

    //    if (emptyParameters.length === 0) {
    //        //remove empty class if everything is entered
    //        jQuery('.empty').each(function () {
    //            jQuery(this).removeClass('empty');
    //        });
    //        //hide error message when all fields are entered
    //        jQuery('#ErrorLabel').hide();

    //        //ajax call to create girl account
    //        var dataToSend = JSON.stringify({
    //            'troopID': localStorage.getItem('troopID'),
    //            'girlID': localStorage.getItem('girlID'),
    //            'FirstName': jQuery('#FirstName').val(),
    //            'LastName': jQuery('#LastName').val(),
    //            'Email': jQuery('#Email').val(),
    //            'Address1': jQuery('#Address1').val(),
    //            'Address2': jQuery('#Address2').val(),
    //            'City': jQuery('#City').val(),
    //            'State': jQuery('#State').val(),
    //            'Zip': jQuery('#Zip').val(),
    //            'Username': jQuery('#Username').val(),
    //            'Password': jQuery('#Password').val(),
    //            'Relationship' : "GRD2"
    //        });
    //        jQuery.ajax({
    //            type: "POST",
    //            url: "https://gsnenytest.imiscloud.com/GSNENY/Membership/Bridge/Bridge.aspx/AddAdultContact",
    //            contentType: "application/json;",
    //            dataType: "json",
    //            data: dataToSend,
    //            success: function (response) {
    //                console.log(response);
    //                if (response.d != null) {
    //                    console.log(response.d);
    //                    if (response.d === 'WebLoginAlreadyExists') {
    //                        jQuery('#ErrorLabel').html('');
    //                        jQuery('#ErrorLabel').append('Someone already has that username. Try another.');
    //                        jQuery('#Username').addClass('empty');
    //                        jQuery('#ErrorLabel').show();
    //                    }
    //                    else if (response.d === 'UserAlreadyExists') {
    //                        jQuery('#ErrorLabel').html('');
    //                        jQuery('#ErrorLabel').append('User already exists in the system.');
    //                        jQuery('#ErrorLabel').show();
    //                    }
    //                    else {
    //                        jQuery('#ErrorLabel').hide();
    //                        jQuery('#Username').removeClass('empty');
    //                        jQuery('#FirstName').val("");
    //                        jQuery('#Email').val("");
    //                        jQuery('#LastName').val("");
    //                        jQuery('#Username').val("");
    //                        jQuery('#Password').val("");
    //                        jQuery('#ConfirmPassword').val("");
    //                }
    //                }
    //            },
    //            error: function (jqXHR, textStatus, errorThrown) {
    //                var obj = JSON.parse(jqXHR.responseText);
    //                console.log(obj.Message);
    //                console.log(jqXHR.statusText);
    //            }
    //        });
    //    }
    //    else {
    //        jQuery('#ErrorLabel').html('');
    //        //getting fields that are empty and appending to the error label, adding empty class
    //        for (var i in emptyParameters) {
    //            jQuery('#ErrorLabel').append(emptyParameters[i] + ', ');;
    //            jQuery('#' + emptyParameters[i]).addClass('empty');
    //        }
    //        jQuery("#ErrorLabel").append(' fields are missing ');
    //        jQuery('#ErrorLabel').show();
    //    }
    //});


    jQuery('#finishButton').on('click', function () {
        var emptyParameters = [];
        //checking if all elements with required class are entered
        jQuery('.required').each(function () {
            if (this.value.length <= 0) {
                emptyParameters.push(this.id);
            }
        });

        if (emptyParameters.length === 0) {
            //remove empty class if everything is entered
            jQuery('.empty').each(function () {
                jQuery(this).removeClass('empty');
            });
            //hide error message when all fields are entered
            jQuery('#ErrorLabel').hide();

            //ajax call to create girl account
            var dataToSend = JSON.stringify({
                'troopID': localStorage.getItem('troopID'),
                'girlID': localStorage.getItem('girlID'),
                'FirstName': jQuery('#FirstName').val(),
                'LastName': jQuery('#LastName').val(),
                'Email': jQuery('#Email').val(),
                'Address1': jQuery('#Address1').val(),
                'Address2': jQuery('#Address2').val(),
                'City': jQuery('#City').val(),
                'State': jQuery('#State').val(),
                'Zip': jQuery('#Zip').val(),
                'Username': jQuery('#Username').val(),
                'Password': jQuery('#Password').val(),
                'Relationship': "GRD2"
            });
            jQuery.ajax({
                type: "POST",
                url: "/GSNENY/Membership/Bridge/Bridge.aspx/AddAdultContact",
                contentType: "application/json;",
                dataType: "json",
                data: dataToSend,
                success: function (response) {
                    console.log(response);
                    if (response.d != null) {

                        if (response.d === 'WebLoginAlreadyExists') {
                            jQuery('#ErrorLabel').html('');
                            jQuery('#ErrorLabel').append('Someone already has that username. Try another.');
                            jQuery('#Username').addClass('empty');
                            jQuery('#ErrorLabel').show();
                        }
                        else if (response.d === 'UserAlreadyExists') {
                            jQuery('#ErrorLabel').html('');
                            jQuery('#ErrorLabel').append('User already exists in the system.');
                            jQuery('#ErrorLabel').show();
                        }
                        else {

                            window.location.href = "/GSNENY/GSNENY/Contacts/ContactLayouts/Girl_AccountPage.aspx?ID=" + localStorage.getItem('girlID');
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var obj = JSON.parse(jqXHR.responseText);
                    console.log(obj.Message);
                    console.log(jqXHR.statusText);
                }
            });
        }
        else {
            jQuery('#ErrorLabel').html('');
            //getting fields that are empty and appending to the error label, adding empty class
            for (var i in emptyParameters) {
                jQuery("#ErrorLabel").append(emptyParameters[i] + ', ');
                jQuery('#' + emptyParameters[i]).addClass('empty');
            }
            jQuery("#ErrorLabel").append(' fields are missing ');
            jQuery('#ErrorLabel').show();
        }
    });
</script>