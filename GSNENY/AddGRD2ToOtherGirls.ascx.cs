﻿using Asi.Soa.ClientServices;
using System;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace GSNENY
{
    public partial class AddGRD2ToOtherGirls : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ID = "", GRD2 = "";
            string connectionString = WebConfigurationManager.ConnectionStrings["DataSource.iMIS.Connection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("select ID from relationship where TARGET_ID = @ID and TARGET_RELATION_TYPE = @RELATION_TYPE", conn);
                    cmd.Parameters.AddWithValue("@ID", Asi.Security.Utility.SecurityHelper.GetSelectedImisId());
                    cmd.Parameters.AddWithValue("@RELATION_TYPE", "CHL");

                    SqlDataReader grdReader = cmd.ExecuteReader();
                    while (grdReader.Read())
                    {
                        ID = grdReader["ID"] != DBNull.Value ? (string)grdReader["ID"] : "";
                        if(ID != "" && ID != Request.QueryString["ID"])
                        {
                            SqlCommand cmd2 = new SqlCommand("select TARGET_ID from relationship where ID = @ID and RELATION_TYPE = @RELATION_TYPE", conn);
                            cmd2.Parameters.AddWithValue("@ID", ID);
                            cmd2.Parameters.AddWithValue("@RELATION_TYPE", "GRD2");

                            SqlDataReader grd2Reader = cmd2.ExecuteReader();
                            if (grd2Reader.Read())
                            {
                                GRD2 = grd2Reader["TARGET_ID"] != DBNull.Value ? (string)grd2Reader["TARGET_ID"] : "";
                                break;
                            }
                        }
                    }
                    if(GRD2 != "")
                    {
                        EntityManager em = new EntityManager("MANAGER");
                        //Response.Write("GRD2 " + GRD2);
                        //AddGrd2.Visible = true;
                        //PartyData partyData = em.FindByIdentity(new IdentityData("Party", GRD2)) as PartyData;
                        //AddGrd2.Text = "Add " + partyData.Name + " as Guardian";
                        //AddGrd2.Attributes.Add("data-id", GRD2);
                    }

                }
                catch (Exception err)
                {
                    Response.Write(err.Message + " || " + err.StackTrace);
                    //throw new Exception(err.Message + " || " + err.StackTrace);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        protected void AddGRD2Relationship(object ssender, EventArgs e)
        {
            
        }
    }
}