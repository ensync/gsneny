﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddGRD2ToOtherGirls.ascx.cs" Inherits="GSNENY.AddGRD2ToOtherGirls" %>
 <div>
     <p style="text-align: right;">
         <input type="button" class="CheckGuardian btn PrimaryButton" id="addGuardianButton" value="Add Guardian" onclick="window.location = '/GSNENY/Contacts/ContactLayouts/Add_Guardian.aspx?ID=' + localStorage.getItem('girlID')">&nbsp;
         <input type="button" class="btn PrimaryButton" id="newContactButton" value="Add New Contact" onclick="window.location = '/GSNENY/Contacts/ContactLayouts/Add_Contacts.aspx?ID=' + localStorage.getItem('girlID')">&nbsp;
     </p>
     <asp:Button runat="server" ID="AddGrd2" style="margin-left:10px" clientIDMode="static" OnClick="AddGRD2Relationship" Visible="false" Text="Add" data-id = "data" CssClass="btn PrimaryButton pull-right CheckGuardian" />
     <%--<input type="button" ID="Button1" style="margin-left:10px;display:none"" value="Add" data-id = "data" class="btn PrimaryButton pull-right CheckGuardian" />--%>
     </div>
<script>
    jQuery(document).ready(function () {
        jQuery('#AddGrd2').attr('onclick', 'AddRelationship();');
        //GetGuardian2();
    });

    //var GetGuardian2 = function () {
    //    var dataToSend = JSON.stringify({
    //        'girlID': localStorage.getItem('girlID'),
    //        'relationship': 'GRD2',
    //    });
    //    jQuery.ajax({
    //        type: "POST",
    //        url: "https://gsnenytest.imiscloud.com/GSNENY/Membership/Bridge/Bridge.aspx/GetGuardian2",
    //        contentType: "application/json;",
    //        dataType: "json",
    //        data: dataToSend,
    //        success: function (response) {
    //            if (response.d != null) {
    //                jQuery.each(response.d, function (index, value) {
                        
    //                });
    //                console.log(response.d["name"]);
    //                console.log(response.d["id"]);
    //            }
    //        },
    //        error: function (jqXHR, textStatus, errorThrown) {
    //            var obj = JSON.parse(jqXHR.responseText);
    //            console.log(obj.Message);
    //            console.log(jqXHR.statusText);
    //        }
    //    });
    //}
    var AddRelationship = function () {
        var dataToSend = JSON.stringify({
            'ID': localStorage.getItem('girlID'),
            'targetID': jQuery('#AddGrd2').attr('data-id'),
            'relationship': 'GRD2',
            'reciprocal': 'CHL'
        });
        jQuery.ajax({
            type: "POST",
            url: '/GSNENY/Membership/Bridge/Bridge.aspx/AddRelationship',
            contentType: "application/json;",
            dataType: "json",
            data: dataToSend,
            success: function (response) {
                location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var obj = JSON.parse(jqXHR.responseText);
                console.log(obj.Message);
                console.log(jqXHR.statusText);
            }
        });
    }
    </script>