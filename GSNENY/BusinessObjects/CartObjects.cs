﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GSNENY.BusinessObjects
{
    public class CartObjects
    {
    }

    public class ComboOrder
    {
        public string type { get; set; }
        public Order Order { get; set; }
    }

    public class Currency
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata ExtensionData { get; set; }
    }

    public class Extensiondata
    {
        public string type { get; set; }
    }

    public class Order
    {
        public Lines Lines { get; set; }
    }

    public class Billtocustomerparty
    {
        public string type { get; set; }
        public Party Party { get; set; }
        public string PartyId { get; set; }
    }

    public class Party
    {
        public string type { get; set; }
        public Personname PersonName { get; set; }
        public Primaryorganization PrimaryOrganization { get; set; }
        public Additionalattributes AdditionalAttributes { get; set; }
        public Addresses Addresses { get; set; }
        public Alternateids AlternateIds { get; set; }
        public Emails Emails { get; set; }
        public Financialinformation FinancialInformation { get; set; }
        public Salutations Salutations { get; set; }
        public Socialnetworks SocialNetworks { get; set; }
        public Communicationtypepreferences CommunicationTypePreferences { get; set; }
        public bool SortIsOverridden { get; set; }
        public Updateinformation UpdateInformation { get; set; }
        public string PartyId { get; set; }
        public string Id { get; set; }
        public string UniformId { get; set; }
        public Status Status { get; set; }
        public string Name { get; set; }
        public string Sort { get; set; }
    }

    public class Personname
    {
        public string type { get; set; }
        public string FirstName { get; set; }
        public string InformalName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
    }

    public class Primaryorganization
    {
        public string type { get; set; }
        public string OrganizationPartyId { get; set; }
        public string Name { get; set; }
    }

    public class Additionalattributes
    {
        public string type { get; set; }
        public Values[] values { get; set; }
    }

    public class Values
    {
        public string type { get; set; }
        public string Name { get; set; }
        public object Value { get; set; }
    }

    public class Addresses
    {
        public string type { get; set; }
        public Values1[] values { get; set; }
    }

    public class Values1
    {
        public string type { get; set; }
        public Additionallines AdditionalLines { get; set; }
        public Address Address { get; set; }
        public string AddresseeText { get; set; }
        public string AddressPurpose { get; set; }
        public Communicationpreferences CommunicationPreferences { get; set; }
        public string Email { get; set; }
        public string FullAddressId { get; set; }
        public Salutation Salutation { get; set; }
        public string DisplayName { get; set; }
        public string DisplayOrganizationName { get; set; }
    }

    public class Additionallines
    {
        public string type { get; set; }
        public object[] values { get; set; }
    }

    public class Address
    {
        public string type { get; set; }
        public string AddressId { get; set; }
        public Addresslines AddressLines { get; set; }
        public string CityName { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string CountrySubEntityCode { get; set; }
        public string CountrySubEntityName { get; set; }
        public string FullAddress { get; set; }
        public string PostalCode { get; set; }
        public int VerificationStatus { get; set; }
    }

    public class Addresslines
    {
        public string type { get; set; }
        public string[] values { get; set; }
    }

    public class Communicationpreferences
    {
        public string type { get; set; }
        public Values2[] values { get; set; }
    }

    public class Values2
    {
        public string type { get; set; }
        public string Reason { get; set; }
    }

    public class Salutation
    {
        public string type { get; set; }
        public Salutationmethod SalutationMethod { get; set; }
        public string Text { get; set; }
    }

    public class Salutationmethod
    {
        public string type { get; set; }
        public string PartySalutationMethodId { get; set; }
    }

    public class Alternateids
    {
        public string type { get; set; }
        public Values3[] values { get; set; }
    }

    public class Values3
    {
        public string type { get; set; }
        public string Id { get; set; }
        public string IdType { get; set; }
    }

    public class Emails
    {
        public string type { get; set; }
        public Values4[] values { get; set; }
    }

    public class Values4
    {
        public string type { get; set; }
        public string Address { get; set; }
        public string EmailType { get; set; }
        public bool IsPrimary { get; set; }
    }

    public class Financialinformation
    {
        public string type { get; set; }
    }

    public class Salutations
    {
        public string type { get; set; }
        public Values5[] values { get; set; }
    }

    public class Values5
    {
        public string type { get; set; }
        public string SalutationId { get; set; }
        public Salutationmethod1 SalutationMethod { get; set; }
        public string Text { get; set; }
    }

    public class Salutationmethod1
    {
        public string type { get; set; }
        public string PartySalutationMethodId { get; set; }
    }

    public class Socialnetworks
    {
        public string type { get; set; }
        public object[] values { get; set; }
    }

    public class Communicationtypepreferences
    {
        public string type { get; set; }
        public object[] values { get; set; }
    }

    public class Updateinformation
    {
        public string type { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
    }

    public class Status
    {
        public string type { get; set; }
        public string PartyStatusId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class Currency1
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata1 ExtensionData { get; set; }
    }

    public class Extensiondata1
    {
        public string type { get; set; }
    }

    //public class Delivery
    //{
    //    public string type { get; set; }
    //    public Values6[] values { get; set; }
    //}

    //public class Values6
    //{
    //    public string type { get; set; }
    //    public Address1 Address { get; set; }
    //    public string AddressId { get; set; }
    //    public Customerparty CustomerParty { get; set; }
    //    public string DeliveryId { get; set; }
    //}

    public class Address1
    {
        public string type { get; set; }
        public Additionallines1 AdditionalLines { get; set; }
        public Address2 Address { get; set; }
        public string AddresseeText { get; set; }
        public string AddressPurpose { get; set; }
        public Communicationpreferences1 CommunicationPreferences { get; set; }
        public string Email { get; set; }
        public string FullAddressId { get; set; }
        public Salutation1 Salutation { get; set; }
        public string DisplayName { get; set; }
        public string DisplayOrganizationName { get; set; }
        public string Phone { get; set; }
    }

    public class Additionallines1
    {
        public string type { get; set; }
        public object[] values { get; set; }
    }

    public class Address2
    {
        public string type { get; set; }
        public string AddressId { get; set; }
        public Addresslines1 AddressLines { get; set; }
        public string CityName { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string CountrySubEntityCode { get; set; }
        public string CountrySubEntityName { get; set; }
        public string FullAddress { get; set; }
        public string PostalCode { get; set; }
        public int VerificationStatus { get; set; }
        public string Barcode { get; set; }
        public string CountyName { get; set; }
    }

    public class Addresslines1
    {
        public string type { get; set; }
        public string[] values { get; set; }
    }

    public class Communicationpreferences1
    {
        public string type { get; set; }
        public Values7[] values { get; set; }
    }

    public class Values7
    {
        public string type { get; set; }
        public string Reason { get; set; }
    }

    public class Salutation1
    {
        public string type { get; set; }
        public Salutationmethod2 SalutationMethod { get; set; }
        public string Text { get; set; }
    }

    public class Salutationmethod2
    {
        public string type { get; set; }
        public string PartySalutationMethodId { get; set; }
    }

    //public class Customerparty
    //{
    //    public string type { get; set; }
    //    public string PartyId { get; set; }
    //}

    public class Orderdiscount
    {
        public string type { get; set; }
        public Currency2 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency2
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata2 ExtensionData { get; set; }
    }

    public class Extensiondata2
    {
        public string type { get; set; }
    }

    public class Linediscounttotal
    {
        public string type { get; set; }
        public Currency3 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency3
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata3 ExtensionData { get; set; }
    }

    public class Extensiondata3
    {
        public string type { get; set; }
    }

    public class Lines
    {
        public string type { get; set; }
        public Values8[] values { get; set; }
    }

    public class Values8
    {
        public Item3 Item { get; set; }
    }

    public class Additionalattributes1
    {
        public string type { get; set; }
        public Values9[] values { get; set; }
    }

    public class Values9
    {
        public string type { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class Childorderlines
    {
        public string type { get; set; }
        public Values10[] values { get; set; }
    }

    public class Values10
    {
        public string type { get; set; }
        public string OrderLineId { get; set; }
        public Childorderlines1 ChildOrderLines { get; set; }
        public Extendedamount1 ExtendedAmount { get; set; }
        public Item1 Item { get; set; }
        public Quantitybackordered1 QuantityBackordered { get; set; }
        public Quantityordered1 QuantityOrdered { get; set; }
        public Quantityshipped1 QuantityShipped { get; set; }
        public Unitprice1 UnitPrice { get; set; }
    }

    public class Childorderlines1
    {
        public string type { get; set; }
        public Values11[] values { get; set; }
    }

    public class Values11
    {
        public string type { get; set; }
        public string OrderLineId { get; set; }
        public Extendedamount ExtendedAmount { get; set; }
        public Item Item { get; set; }
        public Quantitybackordered QuantityBackordered { get; set; }
        public Quantityordered QuantityOrdered { get; set; }
        public Quantityshipped QuantityShipped { get; set; }
        public Unitprice UnitPrice { get; set; }
    }

    public class Extendedamount
    {
        public string type { get; set; }
        public float Amount { get; set; }
        public Currency4 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency4
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata4 ExtensionData { get; set; }
    }

    public class Extensiondata4
    {
        public string type { get; set; }
    }

    public class Item
    {
        public string type { get; set; }
        public Itemclass ItemClass { get; set; }
        public string ItemCode { get; set; }
        public string ItemId { get; set; }
        public string Name { get; set; }
    }

    public class Itemclass
    {
        public string type { get; set; }
        public string ItemClassId { get; set; }
    }

    public class Quantitybackordered
    {
        public string type { get; set; }
    }

    public class Quantityordered
    {
        public string type { get; set; }
        public float Amount { get; set; }
    }

    public class Quantityshipped
    {
        public string type { get; set; }
        public float Amount { get; set; }
    }

    public class Unitprice
    {
        public string type { get; set; }
        public float Amount { get; set; }
        public Currency5 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency5
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public object ExtensionData { get; set; }
    }

    public class Extendedamount1
    {
        public string type { get; set; }
        public float Amount { get; set; }
        public Currency6 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency6
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata5 ExtensionData { get; set; }
    }

    public class Extensiondata5
    {
        public string type { get; set; }
    }

    public class Item1
    {
        public string type { get; set; }
        public Components Components { get; set; }
        public int ItemSetType { get; set; }
        public Itemclass2 ItemClass { get; set; }
        public string ItemCode { get; set; }
        public string ItemId { get; set; }
        public string Name { get; set; }
    }

    public class Components
    {
        public string type { get; set; }
        public Values12[] values { get; set; }
    }

    public class Values12
    {
        public string type { get; set; }
        public bool IsSelected { get; set; }
        public Item2 Item { get; set; }
        public string ItemSetComponentId { get; set; }
        public Maximumquantity MaximumQuantity { get; set; }
        public Quantity Quantity { get; set; }
    }

    public class Item2
    {
        public string type { get; set; }
        public string Description { get; set; }
        public Itemclass1 ItemClass { get; set; }
        public string ItemCode { get; set; }
        public string ItemId { get; set; }
        public string Name { get; set; }
    }

    public class Itemclass1
    {
        public string type { get; set; }
        public string ItemClassId { get; set; }
        public string Name { get; set; }
    }

    public class Maximumquantity
    {
        public string type { get; set; }
        public float Amount { get; set; }
    }

    public class Quantity
    {
        public string type { get; set; }
        public float Amount { get; set; }
    }

    public class Itemclass2
    {
        public string type { get; set; }
        public string ItemClassId { get; set; }
        public string Name { get; set; }
    }

    public class Quantitybackordered1
    {
        public string type { get; set; }
    }

    public class Quantityordered1
    {
        public string type { get; set; }
        public float Amount { get; set; }
    }

    public class Quantityshipped1
    {
        public string type { get; set; }
        public float Amount { get; set; }
    }

    public class Unitprice1
    {
        public string type { get; set; }
        public float Amount { get; set; }
        public Currency7 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency7
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata6 ExtensionData { get; set; }
    }

    public class Extensiondata6
    {
        public string type { get; set; }
    }

    public class Extendedamount2
    {
        public string type { get; set; }
        public float Amount { get; set; }
        public Currency8 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency8
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata7 ExtensionData { get; set; }
    }

    public class Extensiondata7
    {
        public string type { get; set; }
    }

    public class Item3
    {
        public string type { get; set; }
        public int ItemSetType { get; set; }
        public string Description { get; set; }
        public string ItemCode { get; set; }
        public string ItemId { get; set; }
        public string Name { get; set; }
    }

    public class Components1
    {
        public string type { get; set; }
        public Values13[] values { get; set; }
    }

    public class Values13
    {
        public string type { get; set; }
        public Item4 Item { get; set; }
        public string ItemSetComponentId { get; set; }
        public Quantity2 Quantity { get; set; }
    }

    public class Item4
    {
        public string type { get; set; }
        public Components2 Components { get; set; }
        public int ItemSetType { get; set; }
        public string Description { get; set; }
        public Itemclass4 ItemClass { get; set; }
        public string ItemCode { get; set; }
        public string ItemId { get; set; }
        public string Name { get; set; }
    }

    public class Components2
    {
        public string type { get; set; }
        public Values14[] values { get; set; }
    }

    public class Values14
    {
        public string type { get; set; }
        public bool IsSelected { get; set; }
        public Item5 Item { get; set; }
        public string ItemSetComponentId { get; set; }
        public Maximumquantity1 MaximumQuantity { get; set; }
        public Quantity1 Quantity { get; set; }
    }

    public class Item5
    {
        public string type { get; set; }
        public string Description { get; set; }
        public Itemclass3 ItemClass { get; set; }
        public string ItemCode { get; set; }
        public string ItemId { get; set; }
        public string Name { get; set; }
    }

    public class Itemclass3
    {
        public string type { get; set; }
        public string ItemClassId { get; set; }
        public string Name { get; set; }
    }

    public class Maximumquantity1
    {
        public string type { get; set; }
        public float Amount { get; set; }
    }

    public class Quantity1
    {
        public string type { get; set; }
        public float Amount { get; set; }
    }

    public class Itemclass4
    {
        public string type { get; set; }
        public string ItemClassId { get; set; }
        public string Name { get; set; }
    }

    public class Quantity2
    {
        public string type { get; set; }
        public float Amount { get; set; }
    }

    public class Itemclass5
    {
        public string type { get; set; }
        public string ItemClassId { get; set; }
        public string Name { get; set; }
    }

    public class Quantitybackordered2
    {
        public string type { get; set; }
    }

    public class Quantityordered2
    {
        public string type { get; set; }
        public float Amount { get; set; }
    }

    public class Quantityshipped2
    {
        public string type { get; set; }
        public float Amount { get; set; }
    }

    public class Unitprice2
    {
        public string type { get; set; }
        public float Amount { get; set; }
        public Currency9 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency9
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata8 ExtensionData { get; set; }
    }

    public class Extensiondata8
    {
        public string type { get; set; }
    }

    public class Baseunitprice
    {
        public string type { get; set; }
        public float Amount { get; set; }
        public Currency10 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency10
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata9 ExtensionData { get; set; }
    }

    public class Extensiondata9
    {
        public string type { get; set; }
    }

    public class Discount
    {
        public string type { get; set; }
        public Currency11 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency11
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata10 ExtensionData { get; set; }
    }

    public class Extensiondata10
    {
        public string type { get; set; }
    }

    public class Linetotal
    {
        public string type { get; set; }
        public float Amount { get; set; }
        public Currency12 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency12
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata11 ExtensionData { get; set; }
    }

    public class Extensiondata11
    {
        public string type { get; set; }
    }

    public class Miscellaneouschargestotal
    {
        public string type { get; set; }
        public Currency13 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency13
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata12 ExtensionData { get; set; }
    }

    public class Extensiondata12
    {
        public string type { get; set; }
    }

    public class Ordertotal
    {
        public string type { get; set; }
        public float Amount { get; set; }
        public Currency14 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency14
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata13 ExtensionData { get; set; }
    }

    public class Extensiondata13
    {
        public string type { get; set; }
    }

    public class Originatorcustomerparty
    {
        public string type { get; set; }
        public string PartyId { get; set; }
    }

    public class Shippingtotal
    {
        public string type { get; set; }
        public Currency15 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency15
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata14 ExtensionData { get; set; }
    }

    public class Extensiondata14
    {
        public string type { get; set; }
    }

    public class Soldtocustomerparty
    {
        public string type { get; set; }
        public Party1 Party { get; set; }
        public string PartyId { get; set; }
    }

    public class Party1
    {
        public string type { get; set; }
        public Personname1 PersonName { get; set; }
        public Primaryorganization1 PrimaryOrganization { get; set; }
        public Additionalattributes2 AdditionalAttributes { get; set; }
        public Addresses1 Addresses { get; set; }
        public Alternateids1 AlternateIds { get; set; }
        public Emails1 Emails { get; set; }
        public Financialinformation1 FinancialInformation { get; set; }
        public Salutations1 Salutations { get; set; }
        public Socialnetworks1 SocialNetworks { get; set; }
        public Communicationtypepreferences1 CommunicationTypePreferences { get; set; }
        public bool SortIsOverridden { get; set; }
        public Updateinformation1 UpdateInformation { get; set; }
        public string PartyId { get; set; }
        public string Id { get; set; }
        public string UniformId { get; set; }
        public Status1 Status { get; set; }
        public string Name { get; set; }
        public string Sort { get; set; }
    }

    public class Personname1
    {
        public string type { get; set; }
        public string FirstName { get; set; }
        public string InformalName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
    }

    public class Primaryorganization1
    {
        public string type { get; set; }
        public string OrganizationPartyId { get; set; }
        public string Name { get; set; }
    }

    public class Additionalattributes2
    {
        public string type { get; set; }
        public Values15[] values { get; set; }
    }

    public class Values15
    {
        public string type { get; set; }
        public string Name { get; set; }
        public object Value { get; set; }
    }

    public class Addresses1
    {
        public string type { get; set; }
        public Values16[] values { get; set; }
    }

    public class Values16
    {
        public string type { get; set; }
        public Additionallines2 AdditionalLines { get; set; }
        public Address3 Address { get; set; }
        public string AddresseeText { get; set; }
        public string AddressPurpose { get; set; }
        public Communicationpreferences2 CommunicationPreferences { get; set; }
        public string Email { get; set; }
        public string FullAddressId { get; set; }
        public Salutation2 Salutation { get; set; }
        public string DisplayName { get; set; }
        public string DisplayOrganizationName { get; set; }
    }

    public class Additionallines2
    {
        public string type { get; set; }
        public object[] values { get; set; }
    }

    public class Address3
    {
        public string type { get; set; }
        public string AddressId { get; set; }
        public Addresslines2 AddressLines { get; set; }
        public string CityName { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string CountrySubEntityCode { get; set; }
        public string CountrySubEntityName { get; set; }
        public string FullAddress { get; set; }
        public string PostalCode { get; set; }
        public int VerificationStatus { get; set; }
    }

    public class Addresslines2
    {
        public string type { get; set; }
        public string[] values { get; set; }
    }

    public class Communicationpreferences2
    {
        public string type { get; set; }
        public Values17[] values { get; set; }
    }

    public class Values17
    {
        public string type { get; set; }
        public string Reason { get; set; }
    }

    public class Salutation2
    {
        public string type { get; set; }
        public Salutationmethod3 SalutationMethod { get; set; }
        public string Text { get; set; }
    }

    public class Salutationmethod3
    {
        public string type { get; set; }
        public string PartySalutationMethodId { get; set; }
    }

    public class Alternateids1
    {
        public string type { get; set; }
        public Values18[] values { get; set; }
    }

    public class Values18
    {
        public string type { get; set; }
        public string Id { get; set; }
        public string IdType { get; set; }
    }

    public class Emails1
    {
        public string type { get; set; }
        public Values19[] values { get; set; }
    }

    public class Values19
    {
        public string type { get; set; }
        public string Address { get; set; }
        public string EmailType { get; set; }
        public bool IsPrimary { get; set; }
    }

    public class Financialinformation1
    {
        public string type { get; set; }
    }

    public class Salutations1
    {
        public string type { get; set; }
        public Values20[] values { get; set; }
    }

    public class Values20
    {
        public string type { get; set; }
        public string SalutationId { get; set; }
        public Salutationmethod4 SalutationMethod { get; set; }
        public string Text { get; set; }
    }

    public class Salutationmethod4
    {
        public string type { get; set; }
        public string PartySalutationMethodId { get; set; }
    }

    public class Socialnetworks1
    {
        public string type { get; set; }
        public object[] values { get; set; }
    }

    public class Communicationtypepreferences1
    {
        public string type { get; set; }
        public object[] values { get; set; }
    }

    public class Updateinformation1
    {
        public string type { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
    }

    public class Status1
    {
        public string type { get; set; }
        public string PartyStatusId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class Additionalcharges
    {
        public string type { get; set; }
        public Values21[] values { get; set; }
    }

    public class Values21
    {
        public string type { get; set; }
        public string AdditionalChargeId { get; set; }
        public string Description { get; set; }
        public Totalamount TotalAmount { get; set; }
        public Tax Tax { get; set; }
    }

    public class Totalamount
    {
        public string type { get; set; }
        public Currency16 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency16
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata15 ExtensionData { get; set; }
    }

    public class Extensiondata15
    {
        public string type { get; set; }
    }

    public class Tax
    {
        public string type { get; set; }
        public Details Details { get; set; }
        public Taxtotal TaxTotal { get; set; }
        public Inclusivetaxtotal InclusiveTaxTotal { get; set; }
    }

    public class Details
    {
        public string type { get; set; }
        public object[] values { get; set; }
    }

    public class Taxtotal
    {
        public string type { get; set; }
        public Currency17 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency17
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata16 ExtensionData { get; set; }
    }

    public class Extensiondata16
    {
        public string type { get; set; }
    }

    public class Inclusivetaxtotal
    {
        public string type { get; set; }
        public Currency18 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency18
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata17 ExtensionData { get; set; }
    }

    public class Extensiondata17
    {
        public string type { get; set; }
    }

    public class Taxinformation
    {
        public string type { get; set; }
        public Inclusivetaxtotal1 InclusiveTaxTotal { get; set; }
        public Taxtotal1 TaxTotal { get; set; }
        public Ordertaxes OrderTaxes { get; set; }
    }

    public class Inclusivetaxtotal1
    {
        public string type { get; set; }
        public Currency19 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency19
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata18 ExtensionData { get; set; }
    }

    public class Extensiondata18
    {
        public string type { get; set; }
    }

    public class Taxtotal1
    {
        public string type { get; set; }
        public Currency20 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency20
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata19 ExtensionData { get; set; }
    }

    public class Extensiondata19
    {
        public string type { get; set; }
    }

    public class Ordertaxes
    {
        public string type { get; set; }
        public object[] values { get; set; }
    }

    public class Totalbaseprice
    {
        public string type { get; set; }
        public float Amount { get; set; }
        public Currency21 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency21
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata20 ExtensionData { get; set; }
    }

    public class Extensiondata20
    {
        public string type { get; set; }
    }

    public class Additionalattributes3
    {
        public string type { get; set; }
        public Values22[] values { get; set; }
    }

    public class Values22
    {
        public string type { get; set; }
        public string Name { get; set; }
        public Value Value { get; set; }
    }

    public class Value
    {
        public string type { get; set; }
        public float value { get; set; }
    }

    public class Invoices
    {
        public string type { get; set; }
        public object[] values { get; set; }
    }

    public class Payments
    {
        public string type { get; set; }
        public Values23[] values { get; set; }
    }

    public class Values23
    {
        public string type { get; set; }
        public Amount Amount { get; set; }
        public string PaymentToken { get; set; }
        public string PaymentTokenSummary { get; set; }
        public Creditcardinformation CreditCardInformation { get; set; }
        public Paymentmethod PaymentMethod { get; set; }
        public Payorparty PayorParty { get; set; }
        public string ReferenceNumber { get; set; }
        public string Message { get; set; }
    }

    public class Amount
    {
        public string type { get; set; }
        public Currency22 Currency { get; set; }
        public bool IsAmountDefined { get; set; }
    }

    public class Currency22
    {
        public string type { get; set; }
        public string CurrencyCode { get; set; }
        public int DecimalPositions { get; set; }
        public Extensiondata21 ExtensionData { get; set; }
    }

    public class Extensiondata21
    {
        public string type { get; set; }
    }

    public class Creditcardinformation
    {
        public string type { get; set; }
        public string CardNumber { get; set; }
        public Expiration Expiration { get; set; }
        public string HoldersName { get; set; }
        public string SecurityCode { get; set; }
        public Address4 Address { get; set; }
    }

    public class Expiration
    {
        public string type { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }

    public class Address4
    {
        public string type { get; set; }
        public string AddressId { get; set; }
        public Addresslines3 AddressLines { get; set; }
        public string CityName { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string CountrySubEntityCode { get; set; }
        public string CountrySubEntityName { get; set; }
        public string FullAddress { get; set; }
        public string PostalCode { get; set; }
        public int VerificationStatus { get; set; }
    }

    public class Addresslines3
    {
        public string type { get; set; }
        public string[] values { get; set; }
    }

    public class Paymentmethod
    {
        public string type { get; set; }
        public string Name { get; set; }
        public string PaymentMethodId { get; set; }
        public string PaymentType { get; set; }
        public string Message { get; set; }
    }

    public class Payorparty
    {
        public string type { get; set; }
        public string PartyId { get; set; }
    }
}