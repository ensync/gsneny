﻿using GSNENY.BusinessObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GSNENY
{
    public class OrderConfData
    {
        public string type { get; set; }

        public Order Order { get; set; }
    }

    public class Order
    {
        public DateTime OrderDate { get; set; }
        public Lines Lines { get; set; }
        //[JsonProperty("Delivery")]
        //[JsonConverter(typeof(SingleOrArrayConverter<Delivery>))]
        //public List<Delivery> Delivery { get; set; }
    }

    //public class Delivery
    //{
    //    public string type { get; set; }
    //    public Values[] values { get; set; }
    //}

    //public class Values
    //{

    //    public string type { get; set; }
    //    public string AddressId { get; set; }
    //    public Customerparty CustomerParty { get; set; }
    //    public string DeliveryId { get; set; }
    //}

    //public class Customerparty
    //{
    //    public string type { get; set; }
    //    public string PartyId { get; set; }
    //}

    public class Lines
    {
        public string type { get; set; }

        [JsonProperty("values")]
        public Values[] values { get; set; }
    }

    public class Values
    {
        public string OrderLineId { get; set; }
        public Additionalattributes AdditionalAttributes { get; set; }
    }

    public class Additionalattributes
    {
        public string type { get; set; }

        [JsonProperty("values")]
        public AdditionalAttributeValues[] values { get; set; }
    }

    public class AdditionalAttributeValues
    {
        public string type { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
