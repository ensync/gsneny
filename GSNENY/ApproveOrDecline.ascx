﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApproveOrDecline.ascx.cs" Inherits="GSNENY.ApproveOrDecline" %>
<script>
    jQuery(document).ready(function (jQuery) {

        //if status = approved, update Member Type to NonMember Girl
        if (getParameterByName('S').length > 0 && getParameterByName('S') === 'A')
        {
            if(getParameterByName('ID').length > 0)
            {
                UpdateMemberType();
            }
        }
        else if (getParameterByName('S').length > 0 && getParameterByName('S') === 'D') //if status = declined, remove company ID and company name from account
        {
            if (getParameterByName('ID').length > 0) {
                UpdateCompanyID();
            }
        }
    });

    var UpdateMemberType = function () {
        var dataToSend = JSON.stringify({
            'ID': getParameterByName('ID'),
            'MemberType': 'GRNMB'
        });
        jQuery.ajax({
            type: "POST",
            url: '/GSNENY/Membership/Bridge/Bridge.aspx/ApproveGirl',
            contentType: "application/json;",
            data: dataToSend,
            success: function (response) {
                console.log(getParameterByName('WebsiteKey'));
                //console.log("http://gsnenytest.imiscloud.com/GSNENY/Contacts/ContactLayouts/AccountPage.aspx?ID=" + localStorage.getItem('LoggedInUser') + "&WebsiteKey=" + getParameterByName('WebsiteKey'));
                window.location.href = "/GSNENY/Contacts/ContactLayouts/AccountPage.aspx?ID=" + localStorage.getItem('LoggedInUser');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var obj = JSON.parse(jqXHR.responseText);
                console.log(obj.Message);
                console.log(jqXHR.statusText);
            }
        });
    }

    var UpdateCompanyID = function () {
        var dataToSend = JSON.stringify({
            'ID': getParameterByName('ID'),
            'CompanyID': 'REMOVE'
        });
        jQuery.ajax({
            type: "POST",
            url: "/GSNENY/Membership/Bridge/Bridge.aspx/DeclineGirl",
            contentType: "application/json;",
            data: dataToSend,
            success: function (response) {
                //console.log("http://gsnenytest.imiscloud.com/GSNENY/Contacts/ContactLayouts/AccountPage.aspx?ID=" + localStorage.getItem('LoggedInUser') )//+ "&WebsiteKey=" + getParameterByName('WebsiteKey'));
                var redirectURL = "/GSNENY/Contacts/ContactLayouts/AccountPage.aspx?ID=" + localStorage.getItem('LoggedInUser');
                if(getParameterByName('WebsiteKey') !== '')
                    redirectURL = redirectURL + "&WebsiteKey=" + getParameterByName('WebsiteKey');
                window.location.href = redirectURL;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var obj = JSON.parse(jqXHR.responseText);
                console.log(obj.Message);
                console.log(jqXHR.statusText);
            }
        });
    }

    var getParameterByName = function (name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.search);
        if (results == null)
            return "";
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
</script>