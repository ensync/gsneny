﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CaptureTroopIDScript.ascx.cs" Inherits="GSNENY.CaptureTroopIDScript" %>
<script>
    jQuery(document).ready(function (jQuery) {
        console.log('life');
        SetTroopIDLocalStorage();
    });

    var SetTroopIDLocalStorage = function () {
        jQuery.ajax({
            type: "POST",
            url: "/GSNENY/Membership/Bridge/Bridge.aspx/GetTroopIDForLoggedInUser",
            contentType: "application/json;",
            success: function (response) {
                console.log("response " + response.d);
                if (response.d != null) {
                    localStorage.setItem('troopID', response.d);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var obj = JSON.parse(jqXHR.responseText);
                console.log(obj.Message);
                console.log(jqXHR.statusText);
            }
        });
    }
</script>
