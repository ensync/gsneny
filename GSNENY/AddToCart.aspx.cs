﻿using Asi.Soa.ClientServices;
using Asi.Soa.Commerce.DataContracts;
using Asi.Soa.Core.DataContracts;
using Asi.Soa.Membership.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GSNENY
{
    public partial class AddToCart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                EntityManager em = new EntityManager("MANAGER");
                var cartManager = new CartManager(em, Asi.Security.Utility.SecurityHelper.GetSelectedImisId());
                //InvoiceSummaryData invoiceSummaryData1 = new InvoiceSummaryData();
                //InvoiceSummaryData invoiceSummaryData = new InvoiceSummaryData();
                //CurrencyData currency = new CurrencyData();
                //currency.CurrencyCode = "USD";
                //invoiceSummaryData1.AccountingMethod = AccountingMethodData.Cash;
                //invoiceSummaryData1.Balance = new MonetaryAmountData(25, currency);
                //invoiceSummaryData1.BillToParty = (em.FindByIdentity(new IdentityData("Party", Asi.Security.Utility.SecurityHelper.GetSelectedImisId())) as PartyData).ToPartySummary();
                //invoiceSummaryData1.Description = "Girl Dues";
                //invoiceSummaryData1.InvoiceId = "1";
                //invoiceSummaryData1.InvoiceAmount = new MonetaryAmountData(25, currency);
                //invoiceSummaryData1.InvoiceDate = DateTime.Now;
                //invoiceSummaryData1.InvoicePurpose = InvoicePurposeData.SubscriptionBilling;
                //invoiceSummaryData1.SoldToParty = (em.FindByIdentity(new IdentityData("Party", "234689")) as PartyData).ToPartySummary();

                  ComboOrderManager comboOrderManager = new ComboOrderManager(em);
                  foreach (InvoiceSummaryData id in comboOrderManager.GetUnpaidInvoices("234675"))
                  //foreach (InvoiceSummaryData id in comboOrderManager.GetUnpaidInvoices(Asi.Security.Utility.SecurityHelper.GetSelectedImisId()))
                  {
                      Response.Write("id " + id.InvoiceAmount);
                      cartManager.AddInvoice(id);
                  }
                  em.Update(cartManager.Cart);
                //Response.Write("l billbegin " + BillBegin);
            }
            catch (Exception err)
            {
                Response.Write(err.Message + " || " + err.StackTrace);
                //throw new Exception(err.Message + " || " + err.StackTrace);
            }
        }
    }
}