﻿<%@ Control Language="C#" AutoEventWireup="true" Debug="true" CodeFile="CreateScoutRegisterProcess.ascx.cs" Inherits="GSNENY.CreateScoutRegisterProcess" %>

<body>
    <style>

        label.field-wrapper input {
    float: right;
}
label.required-field::before { 
    content: "*";
    float: left;
    color: red;
}
    .empty{
        border-color:red !important;
    }

     .ui-datepicker{
        /*border : 1px solid #aaaaaa !important ;
        background : #ffffff url(https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_flat_75_ffffff_40x100.png) 50% 50% repeat-x !important;
        color: #222222 !important;*/
        width: 14em !important;
    }
        </style>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <div>
        <label style="color: red; display: none" id="ErrorLabel" />
    </div>
    <br>
    <br>
    <div>
      <label  class="field-wrapper required-field col-sm-3" for="FirstName"> First Name </label> <input class="required" type="text" id="FirstName" value=""><br><br>
      <label  class="field-wrapper required-field col-sm-3"> Last Name </label><input class="required" type="text" id="LastName" value="" ><br><br>
      <label  class="field-wrapper required-field col-sm-3"> Grade Upcoming Fall </label><select class="required" id="Grade"></select><br><br>
      <label class="field-wrapper required-field col-sm-3" for="BirthDate">Date of Birth</label><input class="required" id="BirthDate" type="date"><br><br>
      <label  class="field-wrapper required-field col-sm-3"> Phone </label><input class="required" type="text" id="Phone" value=""><br><br>
      <label  class="field-wrapper required-field col-sm-3"> Address 1 </label><input class="required" type="text" id="Address1"><br><br>
      <label  class="col-sm-3"> Address 2 </label><input type="text" id="Address2"><br><br>
      <label  class="field-wrapper required-field col-sm-3"> City </label><input class="required" type="text" id="City" value=""><br><br>
      <label  class="field-wrapper required-field col-sm-3">State </label><select class="required" id="State" ></select><br><br>
      <label  class="field-wrapper required-field col-sm-3">Zip </label><input class="required" type="text" id="Zip" value=""><br><br>
    </div>
    <div class="col-md-8 col-md-offset-4">
        <input type="button" class="PrimaryButton pull-right TextButton" id="finishButton" value="Next" style="margin-left: 10px">
        <br />
    </div>

    <script>
        //(function ($) {
        jQuery(document).ready(function (jQuery) {
            if (localStorage.getItem('troopID') != '') {
                UpdateCompanyID();
                jQuery("#BirthDate").datepicker();
            }
            PopulateGrades();
            PopulateStates();
            //jQuery.when(PopulateStates()).done(function () {

            //});
            //$body = jQuery("body");
            //jQuery(document).on({
            //    ajaxStart: function () {
            //        $body.addClass("loading");
            //        $body.removeClass("removeLoading");
            //    },
            //    ajaxStop: function () {
            //        $body.removeClass("loading");
            //        $body.addClass("removeLoading");
            //    }
            //});

            //var $loading = jQuery('#loading').hide();
            ////Attach the event handler to any element
            //jQuery(document)
            //  .ajaxStart(function () {
            //      //ajax request went so show the loading image
            //      $loading.show();
            //  })
            //.ajaxStop(function () {
            //    //got response so hide the loading image
            //    $loading.hide();
            //});
        });

        var PopulateStates = function () {
            statesDropDown = jQuery('#State');
            jQuery.ajax({
                type: "POST",
                url: "/GSNENY/Membership/Bridge/Bridge.aspx/PopulateStates",
                contentType: "application/json;",
                success: function (response) {
                    //console.log(response.d);
                    if (response.d != null) {
                        statesDropDown.append(jQuery("<option />").val(("")).text(""));
                        jQuery.each(response.d, function (index, value) {
                            statesDropDown.append(jQuery("<option />").val(value).text(index));
                        });
                    }
                    AutoPopulateInformation();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var obj = JSON.parse(jqXHR.responseText);
                    console.log(obj.Message);
                    console.log(jqXHR.statusText);
                }
            });
        }


        var PopulateGrades = function () {
            gradesDropDown = jQuery('#Grade');
            jQuery.ajax({
                type: "POST",
                url: "/GSNENY/Membership/Bridge/Bridge.aspx/PopulateGrades",
                contentType: "application/json;",
                success: function (response) {
                    //console.log(response.d);
                    if (response.d != null) {
                        gradesDropDown.append(jQuery("<option />").val(("")).text(""));
                        var keys = [], grade, i;
                        jQuery.each(response.d, function (index, value) {
                            keys.push(index);
                        });
                        keys.sort();
                        for (i = 0; i < keys.length; i++) {
                            grade = keys[i];
                            gradesDropDown.append(jQuery("<option />").val(grade).text(response.d[grade]));
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var obj = JSON.parse(jqXHR.responseText);
                    console.log(obj.Message);
                    console.log(jqXHR.statusText);
                }
            });
        }


        var AutoPopulateInformation = function () {
            var dataToSend = JSON.stringify({
                'ID': localStorage.getItem('troopID'),
            });
            jQuery.ajax({
                type: "POST",
                url: "/GSNENY/Membership/Bridge/Bridge.aspx/AutoPopulateInformation",
                contentType: "application/json;",
                dataType: "json",
                data: dataToSend,
                success: function (response) {
                    //console.log(response.d);
                    if (response.d != null) {
                        if (response.d.Phone != null) jQuery('#Phone').val(response.d.Phone);
                        if (response.d.Address1 != null) jQuery('#Address1').val(response.d.Address1);
                        if (response.d.Address2 != null) jQuery('#Address2').val(response.d.Address2);
                        if (response.d.City != null) jQuery('#City').val(response.d.City);
                        if (response.d.State != null) jQuery('#State').val(response.d.State);
                        if (response.d.Zip != null) jQuery('#Zip').val(response.d.Zip);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var obj = JSON.parse(jqXHR.responseText);
                    console.log(obj.Message);
                    console.log(jqXHR.statusText);
                }
            });
        }

        var UpdateCompanyID = function () {

            var dataToSend = JSON.stringify({
                'ID': 'LOGGEDINUSER',
                'CompanyID': localStorage.getItem('troopID'),
            });
            jQuery.ajax({
                type: "POST",
                url: "/GSNENY/Membership/Bridge/Bridge.aspx/UpdateCompanyID",
                contentType: "application/json;",
                dataType: "json",
                data: dataToSend,
                success: function (response) {
                    console.log(response.d);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var obj = JSON.parse(jqXHR.responseText);
                    console.log(obj.Message);
                    console.log(jqXHR.statusText);
                }
            });
        }

        jQuery('#finishButton').on('click', function () {
            console.log("finish clicked");

            var emptyParameters = [];
            //checking if all elements with required class are entered
            jQuery('.required').each(function () {
                if (this.value.length <= 0) {
                    emptyParameters.push(this.id);
                }
            });
            if (emptyParameters.length === 0) {
                //remove empty class if everything is entered
                jQuery('.empty').each(function () {
                    jQuery(this).removeClass('empty');
                });
                //hide error message when all fields are entered
                jQuery('#ErrorLabel').hide();

                //ajax call to create girl account

                var dataToSend = JSON.stringify({
                    'FirstName': jQuery('#FirstName').val(),
                    'LastName': jQuery('#LastName').val(),
                    'Grade': jQuery('#Grade').val(),
                    'BirthDate': jQuery('#BirthDate').val(),
                    'Phone': jQuery('#Phone').val(),
                    'Address1': jQuery('#Address1').val(),
                    'Address2': jQuery('#Address2').val(),
                    'City': jQuery('#City').val(),
                    'State': jQuery('#State').val(),
                    'Zip': jQuery('#Zip').val(),
                    'Type': 'Register'
                });
                jQuery.ajax({
                    type: "POST",
                    url: "/GSNENY/Membership/Bridge/Bridge.aspx/AddGirlScout",
                    contentType: "application/json;",
                    dataType: "json",
                    data: dataToSend,
                    success: function (response) {
                        if (response.d === 'UserAlreadyExists') {
                            jQuery('#ErrorLabel').html('');
                            jQuery('#ErrorLabel').append('User already exists in the system.');
                            jQuery('#ErrorLabel').show();
                        }
                        else
                            window.location.href = "https://girlscouts.secure.force.com/girl";
                            //window.location.href = "/GSNENY/Membership/Register/Dues.aspx?ID=" + response.d;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        var obj = JSON.parse(jqXHR.responseText);
                        console.log(obj.Message);
                        console.log(jqXHR.statusText);
                    }
                });
            }
            else {
                jQuery('#ErrorLabel').html('');
                //getting fields that are empty and appending to the error label, adding empty class
                for (var i in emptyParameters) {
                    jQuery("#ErrorLabel").append(emptyParameters[i] + ', ');
                    jQuery('#' + emptyParameters[i]).addClass('empty');
                }
                jQuery("#ErrorLabel").append(' fields are missing ');
                jQuery('#ErrorLabel').show();
            }
        });
        //}); rel="stylesheet"
    </script>
</body>
