﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SetLoggedInUserID_LocalStorage.ascx.cs" Inherits="GSNENY.SetLoggedInUserID_LocalStorage" %>

<script>
    //script on troop leader account page 
    jQuery(document).ready(function () {
        GetLoggedInUserId();
    });

 var GetLoggedInUserId = function () {
        jQuery.ajax({
            type: "POST",
            url: "GSNENY/Membership/Bridge/Bridge.aspx/GetLoggedInUserId",
            contentType: "application/json;",
            success: function (response) {
                if(response.d != null)
                    localStorage.setItem('LoggedInUser', response.d);
             },
            error: function (jqXHR, textStatus, errorThrown) {
                var obj = JSON.parse(jqXHR.responseText);
                console.log(obj.Message);
                console.log(jqXHR.statusText);
            }
        });
    }
    </script>