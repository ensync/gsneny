﻿using Asi.Soa.ClientServices;
using Asi.Soa.Core.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GSNENY
{
    public class BusinessObject
    {
        public BusinessObject()
        {
            //todo use the same entity manager for all requests
        }
        public class UpdateBusinessObjectRequest
        {
            public UpdateBusinessObjectRequest()
            {
                this.Properties = new List<KeyValuePair<string, object>>();
            }
            public string BusinessObjectName { get; set; }
            public List<KeyValuePair<string, object>> Properties { get; set; }
            public string ID { get; set; }
            public string ParentEntity { get; set; }
        }
        public GenericEntityData UpdateBusinessObject(UpdateBusinessObjectRequest request)
        {
            EntityManager em = new EntityManager("MANAGER");

            var recordToUpdate = new Asi.Soa.Core.DataContracts.GenericEntityData(request.BusinessObjectName)
            {
                Identity = new IdentityData(request.BusinessObjectName, request.ID),
                PrimaryParentEntityTypeName = request.ParentEntity,
                PrimaryParentIdentity = new IdentityData(request.ParentEntity, request.ID)
            };
            foreach (var property in request.Properties)
            {
                recordToUpdate.Properties.Add(new GenericPropertyData(property.Key, property.Value));
            }
            recordToUpdate.Properties.Add("ID", request.ID);
            var response = em.Update(recordToUpdate);
            if (!response.IsValid)
            {
                string errorMessage = "";
                foreach (ValidationResultData vrd in response.ValidationResults.Errors)
                {
                    errorMessage += vrd.Message;
                }
                throw new Exception("Message from SOA(" + errorMessage + ")");
            }
            return response.Entity;
        }

        public GenericEntityData AddBusinessObject(UpdateBusinessObjectRequest request)
        {

            EntityManager em = new EntityManager("MANAGER");

            var recordToUpdate = new Asi.Soa.Core.DataContracts.GenericEntityData(request.BusinessObjectName)
            {
                Identity = new IdentityData(request.BusinessObjectName, request.ID),
                PrimaryParentEntityTypeName = request.ParentEntity,
                PrimaryParentIdentity = new IdentityData(request.ParentEntity, request.ID)
            };
            foreach (var property in request.Properties)
            {
                recordToUpdate.Properties.Add(new GenericPropertyData(property.Key, property.Value));
            }
            recordToUpdate.Properties.Add("ID", request.ID);
            var response = em.Add(recordToUpdate);
            if (!response.IsValid)
            {
                string errorMessage = "";
                foreach (ValidationResultData vrd in response.ValidationResults.Errors)
                {
                    errorMessage += vrd.Message;
                }
                throw new Exception("Message from SOA(" + errorMessage + ")");
            }
            return response.Entity;
        }
    }
}