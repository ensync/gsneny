﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SetTroopIDInLocalStorage.ascx.cs" Inherits="GSNENY.SetTroopIDInLocalStorage" %>

<script type="text/javascript">
    //script to be placed on create primary  page so that the troop ID can be set
    jQuery(document).ready(function () {
        if (getParameterByName('ID').length > 0) {
            localStorage.setItem('troopID', getParameterByName('ID'));
        }
    });
    var getParameterByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&amp;]" + name + "=([^&amp;#]*)"),
        results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    </script>