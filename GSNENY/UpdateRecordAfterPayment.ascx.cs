﻿using Asi.iBO.Commerce;
using Asi.iBO.ContactManagement;
using Asi.iBO.ContentManagement;
using Asi.Soa.ClientServices;
using Asi.Soa.Commerce.DataContracts;
using Asi.Soa.Events.DataContracts;
using Asi.Soa.Fundraising.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSNENY.BusinessObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

namespace GSNENY
{
    public partial class UpdateRecordAfterPayment : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    HashSet<string> products = new HashSet<string>();
                    EntityManager em = new EntityManager();
                    //ComboOrderData comboOrderData = (ComboOrderData)Asi.Utilities.DataHelpers.DeserializeJsonString<ComboOrderData>((string)base.Session["Asi.Web.iParts.Commerce.OrderConfirmationDisplay.ComboOrder"]);
                    
                    var comboOrder = (string)base.Session["Asi.Web.iParts.Commerce.OrderConfirmationDisplay.ComboOrder"];
                    if (comboOrder != null)
                    {
                        comboOrder = comboOrder.Replace("$", "");
                        //Response.Write("comboOrder" + comboOrder);

                        //OrderConfData orderConfData = JsonConvert.DeserializeObject<OrderConfData>(comboOrder);

                        OrderConfData orderConfirmationData = JValue.Parse(comboOrder).ToObject<OrderConfData>();
                        //if (orderConfirmationData.Order.Delivery != null)
                        //    foreach (Delivery delivery in orderConfirmationData.Order.Delivery)
                        //    {
                        //        if (delivery.values != null)
                        //        {
                        //            foreach (Values val in delivery.values)
                        //            {
                        //                if (!String.IsNullOrEmpty(val.CustomerParty.PartyId))
                        //                {
                        //                    CWebUser user = CWebUser.LoginByPrincipal(HttpContext.Current.User);
                        //                    CContact contact = new CContact(user, val.CustomerParty.PartyId);
                        //                    DateTime paidThruDate = contact.PaidThroughDate;
                        //                    if (contact.Subscriptions != null)
                        //                    {
                        //                        foreach (CSubscription subs in contact.Subscriptions)
                        //                        {
                        //                            if (subs.ProductTypeCode == "DUES")
                        //                            {
                        //                                subs.PaidThruDate = subs.BillThruDate;
                        //                                paidThruDate = subs.BillThruDate;
                        //                                contact.Save();
                        //                            }
                        //                        }
                        //                        contact.PaidThroughDate = paidThruDate;
                        //                        contact.CustomerTypeCode = "GRMBR";
                        //                        contact.Save();
                        //                    }
                        //                }
                        //            }
                        //        }
                        //    }

                        if (orderConfirmationData.Order.Lines != null)
                        {
                            foreach (Values val in orderConfirmationData.Order.Lines.values)
                            {
                                foreach (AdditionalAttributeValues addval in val.AdditionalAttributes.values)
                                {
                                    string duesURL = addval.Value;
                                    if (duesURL.Contains("ID="))
                                    {
                                        string partyID = Regex.Split(duesURL, "ID=")[1].Split('&')[0];
                                        if (!String.IsNullOrEmpty(partyID))
                                        {
                                            CWebUser user = CWebUser.LoginByPrincipal(HttpContext.Current.User);
                                            CContact contact = new CContact(user, partyID);
                                            //Response.Write("in if " + partyID);

                                            DateTime paidThruDate = contact.PaidThroughDate;
                                            if (contact.Subscriptions != null)
                                            {
                                                foreach (CSubscription subs in contact.Subscriptions)
                                                {
                                                    //Response.Write("in for");
                                                    if (subs.ProductTypeCode == "DUES")
                                                    {
                                                        subs.PaidThruDate = subs.BillThruDate;
                                                        paidThruDate = subs.BillThruDate;
                                                        contact.Save();
                                                    }
                                                }
                                                contact.PaidThroughDate = paidThruDate;
                                                if (contact.CustomerTypeCode == "GRNMB")
                                                    contact.CustomerTypeCode = "GRMBR";
                                                else if (contact.CustomerTypeCode == "ADNMB" || contact.CustomerTypeCode == "PA")
                                                    contact.CustomerTypeCode = "ADMBR";
                                                contact.Save();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception error)
                {
                    Response.Write(error.Message + error.StackTrace);
                }
            }
        }
    }
}