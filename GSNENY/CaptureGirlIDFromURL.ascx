﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CaptureGirlIDFromURL.ascx.cs" Inherits="GSNENY.CaptureGirlIDFromURL" %>

<script type="text/javascript">
    //script on girl account page 
    jQuery(document).ready(function () {
        if (getParameterByName('ID').length > 0) {
            localStorage.setItem('girlID', getParameterByName('ID'));
        }
    });
    var getParameterByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&amp;]" + name + "=([^&amp;#]*)"),
        results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    </script>