﻿<%@ Control Language="C#" AutoEventWireup="true" Debug="true" CodeFile="UpdatePrimaryCareGiverRecord.ascx.cs" Inherits="GSNENY.UpdatePrimaryCareGiverRecord" %>

<script>
    //Update member type of the primary giver to Adult non member
    jQuery(document).ready(function (jQuery) {
        UpdateMemberType();
        //AddCoAdminRelationship();
    });

    var UpdateMemberType = function () {
        var dataToSend = JSON.stringify({
            'ID': 'LOGGEDINUSER',
            'MemberType': 'ADNMB'
        });
        jQuery.ajax({
            type: "POST",
            url: "/GSNENY/Membership/Bridge/Bridge.aspx/UpdateMemberType",
            contentType: "application/json;",
            dataType: "json",
            data: dataToSend,
            success: function (response) {
                console.log(response.d);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var obj = JSON.parse(jqXHR.responseText);
                console.log(obj.Message);
                console.log(jqXHR.statusText);
            }
        });
    }

    var AddCoAdminRelationship = function () {
        var dataToSend = JSON.stringify({
            'ID': localStorage.getItem('troopID'),
            'targetID': 'LOGGEDINUSER',
            'relationship': '_ORG-ADMIN',
            'reciprocal': ''
        });
        jQuery.ajax({
            type: "POST",
            url: "/GSNENY/Membership/Bridge/Bridge.aspx/AddRelationship",
            contentType: "application/json;",
            dataType: "json",
            data: dataToSend,
            success: function (response) {
                //location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var obj = JSON.parse(jqXHR.responseText);
                console.log(obj.Message);
                console.log(jqXHR.statusText);
            }
        });
    }
    </script> 