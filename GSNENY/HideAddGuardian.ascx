﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HideAddGuardian.ascx.cs" Inherits="GSNENY.HideAddGuardian" %>

<script>

    jQuery(document).ready(function (jQuery) {
        jQuery(".CheckGuardian").hide();
        CheckGuardianExists();
    });

    //Check if guardian with relationship "GRD2" exists, if yes, hide add guardian button
    var CheckGuardianExists = function () {
        var dataToSend = JSON.stringify({
            'ID': getParameterByName('ID'),
            'Relationship': 'GRD2'
        });
        jQuery.ajax({
            type: "POST",
            url: "/GSNENY/Membership/Bridge/Bridge.aspx/CheckRelationshipExists",
            contentType: "application/json;",
            data: dataToSend,
            success: function (response) {
                console.log("guardian exists " + response.d);
                if (response.d != null && response.d === false) {
                    jQuery(".CheckGuardian").show();

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var obj = JSON.parse(jqXHR.responseText);
                console.log(obj.Message);
                console.log(jqXHR.statusText);
            }
        });
    }

    var getParameterByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&amp;]" + name + "=([^&amp;#]*)"),
        results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
</script>
