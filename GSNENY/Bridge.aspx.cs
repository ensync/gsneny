﻿using Asi.iBO;
using Asi.iBO.ContactManagement;
using Asi.Soa.ClientServices;
using Asi.Soa.Core.DataContracts;
using Asi.Soa.Membership.DataContracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Configuration;
using System.Web.Services;
using Asi.Soa.Communications.DataContracts;
using System.Text;
using System.Collections;
using Asi.iBO.ContentManagement;
using Asi.Soa.Commerce.DataContracts;

namespace GSNENY
{
    public class ContactInformation
    {
        public string ID { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }

    public partial class Bridge : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static ContactInformation AutoPopulateInformation()
        {
            EntityManager em = new EntityManager("MANAGER");
            PersonData personData = em.FindByIdentity(new IdentityData("Party", Asi.Security.Utility.SecurityHelper.GetSelectedImisId())) as PersonData;

            try
            {

                ContactInformation troopInformation = new ContactInformation();
                //troopInformation.ID = ID;
                troopInformation.Phone = personData.Phone;
                foreach (FullAddressData addressData in personData.Addresses)
                {
                    if (addressData.AddressPurpose == "Primary")
                    {
                        if (addressData.Address != null && addressData.Address.AddressLines != null)
                        {
                            for (int i = 0; i < addressData.Address.AddressLines.Count; i++)
                            {
                                if (i == 0)
                                    troopInformation.Address1 = addressData.Address.AddressLines[i];
                                if (i == 1)
                                    troopInformation.Address2 = addressData.Address.AddressLines[i];
                            }
                        }
                        troopInformation.City = addressData.Address.CityName;
                        troopInformation.State = addressData.Address.CountrySubEntityCode;
                        troopInformation.Zip = addressData.Address.PostalCode;
                    }
                }
                return troopInformation;
            }
            catch (Exception error)
            {
                throw new Exception(error.Message + " || " + error.StackTrace);
            }
        }

        [WebMethod]
        public static bool UpdateMemberType(string ID, string MemberType)
        {
            bool success = false;
            EntityManager em = new EntityManager("MANAGER");
            QueryData nameQuery = new QueryData("Name");
            if (ID == "LOGGEDINUSER")
                nameQuery.AddCriteria(new CriteriaData("PartyId", OperationData.Equal, Asi.Security.Utility.SecurityHelper.GetSelectedImisId()));
            else
                nameQuery.AddCriteria(new CriteriaData("PartyId", OperationData.Equal, ID));
            FindResultsData nameQueryResults = em.Find(nameQuery);

            if (nameQueryResults.Result.Count > 0)
            {
                foreach (GenericEntityData name in nameQueryResults.Result)
                {
                    name.EntityTypeName = "Name";
                    name["MEMBER_TYPE"] = MemberType;
                    ValidateResultsData<GenericEntityData> updateResults;
                    updateResults = em.Update(name);
                    if (updateResults.IsValid) success = true;
                    else
                        foreach (ValidationResultData vrd in updateResults.ValidationResults.Errors)
                        {
                            throw new Exception(vrd.Message);
                        }
                }
            }
            return success;
        }

        [WebMethod]
        public static bool UpdateCompanyID(string ID, string CompanyID)
        {
            bool success = false;
            EntityManager em = new EntityManager("MANAGER");
            QueryData nameQuery = new QueryData("Name");
            if (ID == "LOGGEDINUSER")
                nameQuery.AddCriteria(new CriteriaData("PartyId", OperationData.Equal, Asi.Security.Utility.SecurityHelper.GetSelectedImisId()));
            else
                nameQuery.AddCriteria(new CriteriaData("PartyId", OperationData.Equal, ID));
            FindResultsData nameQueryResults = em.Find(nameQuery);

            if (nameQueryResults.Result.Count > 0)
            {
                foreach (GenericEntityData name in nameQueryResults.Result)
                {
                    name.EntityTypeName = "Name";
                    if ((!String.IsNullOrEmpty(CompanyID)))
                    {
                        if (CompanyID == "REMOVE")
                        {
                            name["CO_ID"] = "";
                            name["COMPANY"] = "";
                            name["COMPANY_SORT"] = "";
                        }
                        else
                        {
                            name["CO_ID"] = CompanyID;
                            PartyData partyData = em.FindByIdentity(new IdentityData("Party", CompanyID)) as PartyData;
                            name["COMPANY"] = partyData.Name;
                        }
                    }
                    ValidateResultsData<GenericEntityData> updateResults;
                    updateResults = em.Update(name);
                    if (updateResults.IsValid) success = true;
                    else
                    {
                        foreach (ValidationResultData vrd in updateResults.ValidationResults.Errors)
                        {
                            throw new Exception(vrd.Message);
                        }
                    }
                }
            }
            return success;
        }

        [WebMethod]
        public static Dictionary<string, string> PopulateStates()
        {
            EntityManager em = new EntityManager("MANAGER");
            Dictionary<string, string> stateList = new Dictionary<string, string>();
            QueryData countrySubEntityQuery = new QueryData("StateProvinceRef");
            countrySubEntityQuery.AddCriteria(new CriteriaData("COUNTRYCODE", OperationData.Equal, "US"));

            FindResultsData countrySubEntityResults = em.Find(countrySubEntityQuery);
            if (countrySubEntityResults != null && countrySubEntityResults.Result.Count > 0)
            {
                foreach (GenericEntityData countrySubEntity in countrySubEntityResults.Result)
                {
                    stateList.Add(countrySubEntity["Description"].ToString(), countrySubEntity["Code"].ToString());
                }
            }
            return stateList;
        }

        [WebMethod]
        public static Dictionary<string, string> PopulateGrades()
        {
            Dictionary<string, string> gradeList = new Dictionary<string, string>();
            try
            {
                EntityManager em = new EntityManager("MANAGER");
                QueryData gradeQuery = new QueryData("GeneralReference");
                gradeQuery.AddCriteria(new CriteriaData("Table", OperationData.Equal, "GRADE"));
                FindResultsData gradeResults = em.Find(gradeQuery);
                if (gradeResults != null && gradeResults.Result.Count > 0)
                {
                    foreach (GenericEntityData gradeEntity in gradeResults.Result)
                    {
                        gradeList.Add(gradeEntity["Name"].ToString(), gradeEntity["description"].ToString());
                    }
                }
            }
            catch (Exception error)
            {
                throw new Exception(error.Message + " || " + error.StackTrace);
            }
            return gradeList;
        }

        [WebMethod]
        public static string AddGirlScout(string FirstName, string LastName, string Grade, DateTime BirthDate, string Phone, string Address1, string Address2, string City, string State, string Zip, string Type)
        {
            GenericEntityData response = null;
            string returnValue = "";
            try
            {
                bool userAlreadyExists = DuplicateContactCheck(BirthDate, LastName, Zip);

                if (userAlreadyExists)
                {
                    return ("UserAlreadyExists");
                }
                else
                {
                    //current app principal should not be set to the logged-in user , so send false as a parameter.
                    IiMISUser user = CStaffUser.LoginByUserId("manager", false);
                    CContact newContact = new CContact(user);
                    string troopID = "";
                    if (!Type.ToLower().Equals("join"))
                        troopID = GetTroopIDForLoggedInUser();
                    newContact.CustomerTypeCode = "P";
                    if (troopID.Length > 0)
                        newContact.InstituteContactId = troopID;
                    if (FirstName.Length > 0)
                        newContact.FirstName = FirstName;
                    if (LastName.Length > 0)
                        newContact.LastName = LastName;
                    if (Address1.Length > 0)
                    {
                        newContact.DefaultAddress.Address1 = Address1;
                        newContact.DefaultAddress.Address2 = Address2;
                        newContact.DefaultAddress.City = City;
                        newContact.DefaultAddress.StateProvince = State;
                        newContact.DefaultAddress.PostalCode = Zip;
                    }
                    if (Phone.Length > 0)
                        newContact.DefaultAddress.Phone = Phone;
                    if (BirthDate != null)
                        newContact.BirthDate = BirthDate;
                    newContact.Save();

                    string saveError = newContact.Errors.PrimaryErrorMessage;

                    if (newContact.Errors.Count > 0)
                    {
                        throw new Exception(newContact.Errors.PrimaryErrorMessage);
                    }
                    else
                    {
                        var businessObject = new BusinessObject();
                        var request = new GSNENY.BusinessObject.UpdateBusinessObjectRequest()
                        {
                            BusinessObjectName = "CsDemographicsGSNENY",
                            ParentEntity = "Party",
                            ID = newContact.ContactId
                        };
                        request.Properties.Add(new KeyValuePair<string, object>("D_Grade", Grade));
                        response = businessObject.UpdateBusinessObject(request);
                        //throw new Exception("relationship " + newContact.ContactId + " s "+Asi.Security.Utility.SecurityHelper.GetSelectedImisId());
                        bool retVal = AddRelationship(newContact.ContactId, Asi.Security.Utility.SecurityHelper.GetSelectedImisId(), "GRD1", "CHL");

                        if (!String.IsNullOrEmpty(Type) && Type == "Request")
                        {
                            //send email to guardian
                            EntityManager em = new EntityManager("MANAGER");
                            PartyData guardianData = em.FindByIdentity(new IdentityData("Party", Asi.Security.Utility.SecurityHelper.GetSelectedImisId())) as PartyData;

                            StringBuilder emailContent = new StringBuilder();
                            emailContent.Append("Hi " + guardianData.Name + "!<br/><br/>");
                            emailContent.Append("<span>Welcome Aboard!!! We have received your request to join and would like to thank you for your interest in Girl Scouts of Northeastern New York. Our Leaders and one of our membership specialists will check your request and will get back to you within 48 hours. In the meantime, you can visit our website <a href=" + HttpContext.Current.Request.Url.Host + ">" + HttpContext.Current.Request.Url.Host + "</a> to learn more about our offerings and programs.</span>");
                            emailContent.Append("<br/><br/>Yours in Scouting<br/><br/>");
                            emailContent.Append("Please note that this Request Form is not an official Girl Scout Registration if you wish to register today <a href=" + HttpContext.Current.Request.Url.Host + "/GSNENY/Contacts/ContactLayouts/AccountPage.aspx>click here</a>");
                        
                            SendEmail(Asi.Security.Utility.SecurityHelper.GetSelectedImisId(), emailContent, "Request Notification");

                            //send email to troop leaders
                            string connectionString = WebConfigurationManager.ConnectionStrings["DataSource.iMIS.Connection"].ConnectionString;
                            string troopLeaderID = String.Empty;
                            using (SqlConnection conn = new SqlConnection(connectionString))
                            {
                                conn.Open();
                                SqlCommand cmd = new SqlCommand("select ID from Relationship where TARGET_ID = @ID and RELATION_TYPE = @RELATION_TYPE and TARGET_RELATION_TYPE = @TARGET_RELATION_TYPE", conn);
                                cmd.Parameters.AddWithValue("@ID", troopID);
                                cmd.Parameters.AddWithValue("@RELATION_TYPE", "TRP");
                                cmd.Parameters.AddWithValue("@TARGET_RELATION_TYPE", "LDRIS");
                                PartyData troopData = em.FindByIdentity(new IdentityData("Party", troopID)) as PartyData;
                                SqlDataReader adminReader = cmd.ExecuteReader();
                                while (adminReader.Read())
                                {
                                    troopLeaderID = adminReader["ID"] != DBNull.Value ? (string)adminReader["ID"] : "";

                                    if (!String.IsNullOrEmpty(troopLeaderID))
                                    {
                                        PartyData troopLeaderData = em.FindByIdentity(new IdentityData("Party", troopLeaderID)) as PartyData;
                                        StringBuilder troopEmailContent = new StringBuilder();

                                        troopEmailContent.Append("Hi " + troopLeaderData.Name + "!");
                                        troopEmailContent.Append("<br/>Good news! " + newContact.FirstName + " has just joined your troop " + troopData.Name + ". Here is her info:<br/>");
                                        troopEmailContent.Append("<br/>Name: " + newContact.FullName);
                                        troopEmailContent.Append("<br/>Grade: " + Grade);
                                        troopEmailContent.Append("<br/>Primary Parent/Caregiver: " + guardianData.Name);
                                        troopEmailContent.Append("<br/>Primary Parent/Caregiver phone: " + guardianData.Phone);
                                        troopEmailContent.Append("<br/>Primary Parent/Caregiver email: " + guardianData.Email);
                                        troopEmailContent.Append("<br/><br/><span>Please be sure to reach out to " + newContact.FirstName + "'s parent or caregiver in the next few days. Welcome them and include information about when and where your next meeting will take place and anything else they might need to know</span> ");
                                        troopEmailContent.Append("<br/><br/><span>It's also a great idea to hold a special activity to help new members get to know the rest of the troop/group and for the other girls to get to know them.</span>");
                                        troopEmailContent.Append("<br/><br/><span>Have fun! And please be in touch with any questions.</span>");
                                        troopEmailContent.Append("<br/><br/>Best,<span></span>");
                                        troopEmailContent.Append("<br/><br/>Girl Scouts of Northeastern New York, Inc.<span></span>");
                                        SendEmail(troopLeaderID, troopEmailContent, "Request Notification", "membership@girlscoutsneny.org");
                                    }
                                }
                            }

                            //send email to membership@girlscoutsneny.org
                            //StringBuilder staffEmailContent = new StringBuilder();
                            //staffEmailContent.Append("<br/>Good news! " + newContact.FirstName + " has just joined your troop " + troopID + ". Here is her info:<br/>");
                            //staffEmailContent.Append("<br/>Name: " + newContact.FullName);
                            //staffEmailContent.Append("<br/>Grade: " + Grade);
                            //staffEmailContent.Append("<br/>Primary Parent/Caregiver: " + guardianData.Name);
                            //staffEmailContent.Append("<br/>Primary Parent/Caregiver phone: " + guardianData.Phone);
                            //staffEmailContent.Append("<br/>Primary Parent/Caregiver email: " + guardianData.Email);
                            //staffEmailContent.Append("<br/><br/>Best,<span></span>");
                            //staffEmailContent.Append("<br/><br/>Girl Scouts of Northeastern New York, Inc.<span></span>");
                            //SendEmailToAddress("membership@girlscoutsneny.org", staffEmailContent, "Request Notification");
                        }
                        

                        returnValue = newContact.ContactId;
                    }
                    return returnValue;
                }
            }
            catch (Exception error)
            {
                throw new Exception(error.Message + " || " + error.StackTrace);
                //return (error.Message + " || " + error.StackTrace);
            }
        }

        [WebMethod]
        public static string AddAdultContact(string troopID, string girlID, string FirstName, string LastName, string Email, string Address1, string Address2, string City, string State, string Zip, string Username, string Password, string Relationship)
        {
            try
            {
                DateTime adultDOB = DateTime.MinValue;
                //current app principal should not be set to the logged-in user , so send false as a parameter.
                IiMISUser user = CStaffUser.LoginByUserId("manager", false);
                bool webLoginAlreadyExists = CContactUser.CheckWebLogin(Username);
                bool userAlreadyExists = DuplicateContactCheck(FirstName, LastName, Zip);
                if (webLoginAlreadyExists)
                {
                    return ("WebLoginAlreadyExists");
                }
                else if (userAlreadyExists)
                {
                    return ("UserAlreadyExists");
                }
                else
                {
                    CContact newContact = new CContact(user);
                    newContact.CustomerTypeCode = "ADNMB";
                    newContact.EmailAddress = Email;
                    if (troopID.Length > 0)
                        newContact.InstituteContactId = troopID;
                    if (FirstName.Length > 0)
                        newContact.FirstName = FirstName;
                    if (LastName.Length > 0)
                        newContact.LastName = LastName;
                    if (Address1.Length > 0)
                    {
                        newContact.DefaultAddress.Address1 = Address1;
                        newContact.DefaultAddress.Address2 = Address2;
                        newContact.DefaultAddress.City = City;
                        newContact.DefaultAddress.StateProvince = State;
                        newContact.DefaultAddress.PostalCode = Zip;
                    }
                    newContact.Save();
                    newContact.UserSecurity.ChangeWebLogin(Username, Password);
                    newContact.Save();
                    string saveError = newContact.Errors.PrimaryErrorMessage;
                    if (newContact.Errors.Count > 0)
                    {
                        throw new Exception(newContact.Errors.PrimaryErrorMessage);
                    }
                    else
                    {
                        AddRelationship(girlID, newContact.ContactId, Relationship, "CHL");
                        if (Relationship == "GRD2")
                            AddRelationship(newContact.ContactId, Asi.Security.Utility.SecurityHelper.GetSelectedImisId(), "GRD2", "GRD1");
                        else if (Relationship == "GRDADDL")
                            AddRelationship(newContact.ContactId, Asi.Security.Utility.SecurityHelper.GetSelectedImisId(), "GRD2", "GRD1");
                        //SendEmail();
                    }
                    return ("success");
                }

            }
            catch (Exception error)
            {
                //throw new Exception(error.Message + " || " + error.StackTrace);
                return (error.Message + " || " + error.StackTrace);
            }
            //return added;
        }

        [WebMethod]
        public static bool AddRelationship(string ID, string targetID, string relationship, string reciprocal)
        {
            int counter = 0;
            bool returnVal = false;
            if(ID == "TROOPID")
            {
                ID = GetTroopIDForLoggedInUser();
            }
            if (targetID == "LOGGEDINUSER")
            {
                targetID = Asi.Security.Utility.SecurityHelper.GetSelectedImisId();
            }
            if (!CheckRelationshipExists(ID, targetID, relationship))
            {string connectionString = WebConfigurationManager.ConnectionStrings["DataSource.iMIS.Connection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    try
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "sp_asi_GetCounter";

                        SqlParameter param = new SqlParameter("@counterName", SqlDbType.VarChar);
                        param.Value = "Relationship";
                        param.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(param);

                        SqlDataReader counterReader = cmd.ExecuteReader();
                        while (counterReader.Read())
                        {
                            if (counterReader["LAST_VALUE"] != null)
                            {
                                counter = Int32.Parse(counterReader["LAST_VALUE"].ToString());
                            }
                        }
                        if (counter > 0)
                        {
                            string gradeQuery = "INSERT INTO Relationship(ID, RELATION_TYPE, TARGET_ID, TARGET_RELATION_TYPE, STATUS, DATE_ADDED, UPDATED_BY, SEQN) VALUES ( '" + ID + "', '" + relationship + "', '" + targetID + "', '" + reciprocal + "', 'A', '" + DateTime.Now + "', '" + Asi.Security.Utility.SecurityHelper.GetSelectedImisId() + "', '" + counter + "')"; //" +  + "
                            SqlCommand command = new SqlCommand(gradeQuery, conn);

                            SqlDataReader relationshipReader = command.ExecuteReader();
                            if (relationshipReader.RecordsAffected > 0)
                            {
                                returnVal = true;
                            }
                        }
                    }
                    catch (Exception err)
                    {
                        throw new Exception(counter + err.Message + " || " + err.StackTrace);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
                
            }
            return returnVal;
        }

        public static bool DuplicateContactCheck(DateTime BirthDate, string LastName, string ZipCode)
        {
            string DupMatchKey = String.Empty;
            bool userAlreadyExists = false;
            StringBuilder where = new StringBuilder();
            ArrayList parameters = new ArrayList();

            if (BirthDate != DateTime.MinValue)
            {
                where.Append(" AND BIRTH_DATE = @BirthDate ");
                SqlParameter param = new SqlParameter("@BirthDate", SqlDbType.DateTime);
                param.Value = BirthDate;
                parameters.Add(param);
            }

            where.Append(" AND LAST_NAME = @LastName ");
            where.Append(" AND ZIP = @ZipCode ");
            SqlParameter param1 = new SqlParameter("@LastName", SqlDbType.VarChar);
            param1.Value = LastName;
            parameters.Add(param1);
            SqlParameter param2 = new SqlParameter("@ZipCode", SqlDbType.VarChar);
            param2.Value = ZipCode;
            parameters.Add(param2);

            CWebUser user = CWebUser.LoginByPrincipal(HttpContext.Current.User);
            CContact[] results = CContact.GetContacts(user, string.Empty, where.ToString(), (SqlParameter[])parameters.ToArray(typeof(SqlParameter)));
            if (results.Length == 0)
            {
                userAlreadyExists = false;
            }
            else
            {
                userAlreadyExists = true;
            }

            return userAlreadyExists;
        }

        public static bool DuplicateContactCheck(string FirstName, string LastName, string ZipCode)
        {
            string DupMatchKey = String.Empty;
            bool userAlreadyExists = false;
            StringBuilder where = new StringBuilder();
            ArrayList parameters = new ArrayList();

            if (FirstName != "")
            {
                where.Append(" AND FIRST_NAME = @FirstName ");
                SqlParameter param = new SqlParameter("@FirstName", SqlDbType.VarChar);
                param.Value = FirstName;
                parameters.Add(param);
            }

            where.Append(" AND LAST_NAME = @LastName ");
            where.Append(" AND ZIP = @ZipCode ");
            SqlParameter param1 = new SqlParameter("@LastName", SqlDbType.VarChar);
            param1.Value = LastName;
            parameters.Add(param1);
            SqlParameter param2 = new SqlParameter("@ZipCode", SqlDbType.VarChar);
            param2.Value = ZipCode;
            parameters.Add(param2);

            CWebUser user = CWebUser.LoginByPrincipal(HttpContext.Current.User);
            CContact[] results = CContact.GetContacts(user, string.Empty, where.ToString(), (SqlParameter[])parameters.ToArray(typeof(SqlParameter)));
            if (results.Length == 0)
            {
                userAlreadyExists = false;
            }
            else
            {
                userAlreadyExists = true;
            }

            return userAlreadyExists;
        }

        public static String SendEmail(string ToID, StringBuilder emailContent, String Subject, string CCAddress = "")
        {
            EntityManager em = new EntityManager("MANAGER");
            PartyData partyData = em.FindByIdentity(new IdentityData("Party", ToID)) as PartyData;
            if (partyData != null)
            {
                if (partyData.Emails != null)
                {
                    foreach (EmailData email in partyData.Emails)
                    {
                        if (email.Address.Length > 0 && email.IsPrimary)
                        {
                            MessageData messageDatum = new MessageData();
                            MessageAddressDataCollection emailToCollection = new MessageAddressDataCollection();
                            MessageAddressDataCollection emailToCCCollection = new MessageAddressDataCollection();
                            bool exists = false;

                            MessageAddressData emailToDatum = new MessageAddressData();
                            emailToDatum.Address = email.Address;
                            emailToCollection.Add(emailToDatum);
                            messageDatum.To = emailToCollection;

                            if (!String.IsNullOrEmpty(CCAddress))
                            {
                                MessageAddressData emailToCCDatum = new MessageAddressData();
                                emailToCCDatum.Address = CCAddress;
                                emailToCCCollection.Add(emailToCCDatum);
                                messageDatum.CopyTo = emailToCCCollection;
                            }

                            MessageAddressData emailFromDatum = new MessageAddressData();
                            emailFromDatum.Address = "info@gsneny.org";
                            messageDatum.From = emailFromDatum;

                            messageDatum.MessageType = MessageTypeData.Email;
                            messageDatum.Subject = Subject;
                            messageDatum.Text = emailContent.ToString();
                            messageDatum.IsHtml = true;

                            ValidateResultsData<MessageData> validateResultsDatum = em.Add<MessageData>(messageDatum);
                            exists = !validateResultsDatum.IsValid;
                            if (exists)
                            {
                                foreach (ValidationResultData vrd in validateResultsDatum.ValidationResults.Errors)
                                {
                                    return vrd.Message;
                                }
                            }
                            else
                            {
                                return "Email sent " + email.Address;
                            }
                        }
                    }
                }
            }
            return "Unable to send email";
        }

        public static String SendEmailToAddress(string ToAddress, StringBuilder EmailContent, String Subject, string CCAddress = "")
        {
            EntityManager em = new EntityManager("MANAGER");

            MessageData messageDatum = new MessageData();
            MessageAddressDataCollection emailToCollection = new MessageAddressDataCollection();
            MessageAddressDataCollection emailToCCCollection = new MessageAddressDataCollection();

            bool exists = false;

            MessageAddressData emailToDatum = new MessageAddressData();
            emailToDatum.Address = ToAddress;
            emailToCollection.Add(emailToDatum);
            messageDatum.To = emailToCollection;

            if (!String.IsNullOrEmpty(CCAddress))
            {
                MessageAddressData emailToCCDatum = new MessageAddressData();
                emailToCCDatum.Address = CCAddress;
                emailToCCCollection.Add(emailToCCDatum);
                messageDatum.CopyTo = emailToCCCollection;
            }

            MessageAddressData emailFromDatum = new MessageAddressData();
            emailFromDatum.Address = "info@gsneny.org";
            messageDatum.From = emailFromDatum;

            messageDatum.MessageType = MessageTypeData.Email;
            messageDatum.Subject = Subject;
            messageDatum.Text = EmailContent.ToString();
            messageDatum.IsHtml = true;

            ValidateResultsData<MessageData> validateResultsDatum = em.Add<MessageData>(messageDatum);
            exists = !validateResultsDatum.IsValid;
            if (exists)
            {
                foreach (ValidationResultData vrd in validateResultsDatum.ValidationResults.Errors)
                {
                    return vrd.Message;
                }
            }
            else
            {
                return "Email sent";
            }

            return "Unable to send email";
        }

        //Check if a particular relationship  exists on a particular ID
        [WebMethod]
        public static bool CheckRelationshipExists(string ID, string TargetID, string Relationship)
        {
            int counter = 0; bool returnVal = false; 
            string connectionString = WebConfigurationManager.ConnectionStrings["DataSource.iMIS.Connection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("select count(*) as COUNTER from Relationship where ID = @ID and RELATION_TYPE = @RELATION_TYPE and TARGET_ID = @TARGET_ID", conn);
                    cmd.Parameters.AddWithValue("@ID", ID);
                    cmd.Parameters.AddWithValue("@TARGET_ID", TargetID);
                    cmd.Parameters.AddWithValue("@RELATION_TYPE", Relationship);

                    SqlDataReader relReader = cmd.ExecuteReader();

                    while (relReader.Read())
                    {
                        if (relReader["COUNTER"] != DBNull.Value)
                            counter = (int)relReader["COUNTER"];
                        if (counter > 0)
                        {
                            returnVal = true;
                        }
                    }
                }
                catch (Exception err)
                {
                    throw new Exception(err.Message + " || " + err.StackTrace);
                }
                finally
                {
                    conn.Close();
                }
            }
            return returnVal;
        }

        //Check if a particular relationship exists for a given ID and relationship
        [WebMethod]
        public static bool CheckRelationshipExists(string ID, string Relationship)
        {
            int counter = 0; bool returnVal = false;
            string connectionString = WebConfigurationManager.ConnectionStrings["DataSource.iMIS.Connection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("select count(*) as COUNTER from Relationship where ID = @ID and RELATION_TYPE = @RELATION_TYPE", conn);
                    cmd.Parameters.AddWithValue("@ID", ID);
                    cmd.Parameters.AddWithValue("@RELATION_TYPE", Relationship);

                    SqlDataReader relReader = cmd.ExecuteReader();

                    while (relReader.Read())
                    {
                        if (relReader["COUNTER"] != DBNull.Value)
                            counter = (int)relReader["COUNTER"];
                        if (counter > 0)
                        {
                            returnVal = true; 
                        }
                    }
                }
                catch (Exception err)
                {
                    throw new Exception(err.Message + " || " + err.StackTrace);
                }
                finally
                {
                    conn.Close();
                }
            }
            return returnVal;
        }

        //Check if a particular relationship  exists on a all child records
        [WebMethod]
        public static Dictionary<string,string> GetGuardian2(string girlID, string Relationship)
        {
            string GRD2 = "", ID ="";
            Dictionary<string,string> returnObject = new Dictionary<string,string>();
            string connectionString = WebConfigurationManager.ConnectionStrings["DataSource.iMIS.Connection"].ConnectionString;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("select ID from relationship where TARGET_ID = @ID and TARGET_RELATION_TYPE = @RELATION_TYPE", conn);
                    cmd.Parameters.AddWithValue("@ID", Asi.Security.Utility.SecurityHelper.GetSelectedImisId());
                    cmd.Parameters.AddWithValue("@RELATION_TYPE", "CHL");

                    SqlDataReader grdReader = cmd.ExecuteReader();
                    while (grdReader.Read())
                    {
                        ID = grdReader["ID"] != DBNull.Value ? (string)grdReader["ID"] : "";
                        if (ID != "" && ID != girlID)
                        {
                            SqlCommand cmd2 = new SqlCommand("select TARGET_ID from relationship where ID = @ID and RELATION_TYPE = @RELATION_TYPE", conn);
                            cmd2.Parameters.AddWithValue("@ID", ID);
                            cmd2.Parameters.AddWithValue("@RELATION_TYPE", "GRD2");

                            SqlDataReader grd2Reader = cmd2.ExecuteReader();
                            if (grd2Reader.Read())
                            {
                                GRD2 = grd2Reader["TARGET_ID"] != DBNull.Value ? (string)grd2Reader["TARGET_ID"] : "";
                                break;
                            }
                        }
                    }
                    if (GRD2 != "")
                    {
                        EntityManager em = new EntityManager("MANAGER");
                        PartyData partyData = em.FindByIdentity(new IdentityData("Party", GRD2)) as PartyData;
                        returnObject.Add("id", GRD2);
                        returnObject.Add("name", partyData.Name);
                    }
                }
                catch (Exception err)
                {
                    //Response.Write(err.Message + " || " + err.StackTrace);
                    throw new Exception(err.Message + " || " + err.StackTrace);
                }
                finally
                {
                    conn.Close();
                }
                return returnObject;
            }
        }

        [WebMethod]
        public static string GetPrimaryGuardian(string girlID, string Relationship)
        {
            string primaryGuardianID = "";
            string connectionString = WebConfigurationManager.ConnectionStrings["DataSource.iMIS.Connection"].ConnectionString;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("select TARGET_ID from relationship where ID = @ID and RELATION_TYPE = @RELATION_TYPE", conn);
                    cmd.Parameters.AddWithValue("@ID", girlID);
                    cmd.Parameters.AddWithValue("@RELATION_TYPE", "GRD1");

                    SqlDataReader grdReader = cmd.ExecuteReader();
                    while (grdReader.Read())
                    {
                        primaryGuardianID = grdReader["TARGET_ID"] != DBNull.Value ? (string)grdReader["TARGET_ID"] : "";
                    }
                }
                catch (Exception err)
                {
                    //Response.Write(err.Message + " || " + err.StackTrace);
                    throw new Exception(err.Message + " || " + err.StackTrace);
                }
                finally
                {
                    conn.Close();
                }
                return primaryGuardianID;
            }
        }

        [WebMethod]
        public static string AddToCart(string girlID)
        {
            try
            {
                EntityManager em = new EntityManager("MANAGER");
                var cartManager = new CartManager(em, girlID);
                InvoiceSummaryData invoiceSummaryData1 = new InvoiceSummaryData();
                CurrencyData currency = new CurrencyData();
                currency.CurrencyCode = "USD";
                invoiceSummaryData1.AccountingMethod = AccountingMethodData.Cash;

                invoiceSummaryData1.Balance = new MonetaryAmountData(25, currency);
                invoiceSummaryData1.BillToParty = (em.FindByIdentity(new IdentityData("Party", Asi.Security.Utility.SecurityHelper.GetSelectedImisId())) as PartyData).ToPartySummary();
                invoiceSummaryData1.Description = "Girl Dues";

                invoiceSummaryData1.InvoiceAmount = new MonetaryAmountData(25, currency);
                invoiceSummaryData1.InvoiceDate = DateTime.Now;
                invoiceSummaryData1.InvoicePurpose = InvoicePurposeData.SubscriptionBilling;
                invoiceSummaryData1.SoldToParty = (em.FindByIdentity(new IdentityData("Party", girlID)) as PartyData).ToPartySummary();
                cartManager.AddInvoice(invoiceSummaryData1);
                //Response.Write("l billbegin " + BillBegin);
            }
            catch (Exception err)
            {
                return(err.Message + " || " + err.StackTrace);
                //throw new Exception(err.Message + " || " + err.StackTrace);
            }
            return "success";
        }

        public static string GetTroopIDForLoggedInUser()
        {
            try
            {
                CWebUser loggedInUser = CWebUser.LoginByPrincipal(HttpContext.Current.User);
                Asi.iBO.DataServer companyAdminDS = new Asi.iBO.DataServer(loggedInUser);

                string companyAdminQuery = "SELECT CO_ID from Name where ID = '" + Asi.Security.Utility.SecurityHelper.GetSelectedImisId() + "'";
                SqlDataReader companyAdminReader = companyAdminDS.ExecuteReader(System.Data.CommandType.Text, companyAdminQuery);
                while (companyAdminReader.Read())
                {
                    return companyAdminReader["CO_ID"].ToString();
                }
                return "";
            }
            catch (Exception err)
            {
                //return(err.Message + " || " + err.StackTrace);
                throw new Exception(err.Message + " || " + err.StackTrace);
            }
        }

        [WebMethod]
        public static string GetLoggedInUserId()
        {
            try
            {
                return Asi.Security.Utility.SecurityHelper.GetSelectedImisId();
            }
            catch (Exception err)
            {
                //return(err.Message + " || " + err.StackTrace);
                throw new Exception(err.Message + " || " + err.StackTrace);
            }
        }

        [WebMethod]
        public static bool ApproveGirl(string ID, string MemberType)
        {
            try
            {
                if (UpdateMemberType(ID, MemberType) == true)
                {
                    StringBuilder emailContent = new StringBuilder();
                    emailContent.Append("Welcome to Girl Scouts!");
                    emailContent.Append("<br>Congratulations! You are an official member of the troop. Make sure you complete your registration to be an official member of Girl Scouts. Here are the troop details you've been waiting for:");
                    emailContent.Append("<br>Meetings are held every week.");
                    emailContent.Append("<br>Please confirm you are coming.");
                    emailContent.Append("<br>We can’t wait to see you.");
                    string primaryGuardianID = GetPrimaryGuardian(ID, "GRD1");
                    if (!String.IsNullOrEmpty(primaryGuardianID))
                    {
                        string returnVal = SendEmail(primaryGuardianID, emailContent, "Congratulation!", "membership@gsneny.org");
                        //throw new Exception(returnVal);
                        return true;                    
                    }
                    else throw new Exception("Primary guardian ID is null");
                }
                else throw new Exception("Could not update member type for this ID");
            }
            catch (Exception err)
            {
                //return(err.Message + " || " + err.StackTrace);
                throw new Exception(err.Message + " || " + err.StackTrace);
            }
        }

        [WebMethod]
        public static bool DeclineGirl(string ID, string CompanyID)
        {
            try
            {
                if (UpdateCompanyID(ID, CompanyID) == true)
                {
                    StringBuilder emailContent = new StringBuilder();
                    EntityManager em = new EntityManager("MANAGER");
                    PartyData partyData = em.FindByIdentity(new IdentityData("Party", ID)) as PartyData;
                    emailContent.Append("Dear " + partyData.Name);
                    emailContent.Append("<br>Thank you for your interest in Girl Scouts. The troop you have chosen is at capacity at this point. Please reach out to other troops in the area. Please note that you will be contacted by a membership specialist to assist you to find a troop in your area.");
                    emailContent.Append("<br>Yours in Scouting.");

                    string toId = GetPrimaryGuardian(ID, "GRD1");
                    if (!String.IsNullOrEmpty(toId)) 
                        SendEmail(toId, emailContent, "Full Troop", "membership@gsneny.org");
                    return true;
                }
                else return false;

            }
            catch (Exception err)
            {
                //return(err.Message + " || " + err.StackTrace);
                throw new Exception(err.Message + " || " + err.StackTrace);
            }
        }
        
    }
}